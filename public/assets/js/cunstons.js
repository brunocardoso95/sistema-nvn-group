function validarCNPJ(a) {
    if ("" == (a = a.replace(/[^\d]+/g, ""))) return !1;
    if (14 != a.length || "00000000000000" == a || "11111111111111" == a || "22222222222222" == a || "33333333333333" == a || "44444444444444" == a || "55555555555555" == a || "66666666666666" == a || "77777777777777" == a || "88888888888888" == a || "99999999999999" == a) return !1;
    for (tamanho = a.length - 2, numeros = a.substring(0, tamanho), digitos = a.substring(tamanho), soma = 0, pos = tamanho - 7, i = tamanho; i >= 1; i--) soma += numeros.charAt(tamanho - i) * pos--, pos < 2 && (pos = 9);
    if (resultado = soma % 11 < 2 ? 0 : 11 - soma % 11, resultado != digitos.charAt(0)) return !1;
    for (tamanho += 1, numeros = a.substring(0, tamanho), soma = 0, pos = tamanho - 7, i = tamanho; i >= 1; i--) soma += numeros.charAt(tamanho - i) * pos--, pos < 2 && (pos = 9);
    return resultado = soma % 11 < 2 ? 0 : 11 - soma % 11, resultado == digitos.charAt(1)
}
