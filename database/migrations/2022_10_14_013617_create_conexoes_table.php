<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conexoes', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->integer('status')->default(0);
            $table->string('qrcode')->nullable();
            $table->bigInteger('telefone')->nullable();
            $table->boolean('webhook')->default(false);
            $table->bigInteger('time')->default(20000);
            $table->unsignedBigInteger('empresa_id')->nullable();
            $table->timestamps();

            $table->index(['telefone']);
            $table->index(['webhook']);

            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conexoes');
    }
};
