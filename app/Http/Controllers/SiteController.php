<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SiteController extends Controller
{
    //
    public function index(Request $req) {
        return view('index');
    }

    public function login(Request $req) {
        $login = Auth::check();
        if($login) return redirect()->route('dashboard');
        return view('login');
    }

    public function loginPost(Request $req) {
        $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if (Auth::attempt($credentials)) {
            $u = auth()->user();
            $req->session()->regenerate();
            if($u->status <= 0) Auth::logout();
            return redirect()->intended('dashboard');
        }else{
            \Log::info('The provided credentials do not match our records.');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }
}
