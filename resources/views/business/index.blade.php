@extends('layouts.system')

@section('content')
<div class="container-fluid">
    <nav style="--falcon-breadcrumb-divider: '»';" aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Administrativo</a></li>
        <li class="breadcrumb-item active" aria-current="page">Empresas</li>
        <li class="breadcrumb-item"><a href="#" id="addEmpresa" class="me-1"><i class="align-middle fa-solid fa-add"></i></a></li>
        </ol>
    </nav>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table id="datatables-clients" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>CNPJ</th>
                                <th class='d-none'>Razão Social</th>
                                <th>Nome Fantasia</th>
                                <th>Responsável</th>
                                <th>Vencimento</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($empresas) && count($empresas)>0)
                            @foreach($empresas as $e)
                            <tr>
                                <td>{{$e->maskCnpj()}}</td>
                                <td class='d-none'>{{$e->razaosocial}}</td>
                                <td>{{$e->fantasia}}</td>
                                <td><a href="http://wa.me/55{{$e->celular}}"><i class="fa-brands fa-whatsapp me-1"></i></a>{{$e->responsavel}}</td>
                                <td>{{$e->vencimentoDate()}}</td>
                                <td>
                                    <button class="btn btn-sm btn-default editEmpresa" data-empresa="{{$e->id}}" data-url="{{route('business.create', ['id' => $e->id ])}}"><i class="fas fa-pen"></i></button>
                                    <button class="btn btn-sm btn-danger deleteEmpresa" data-empresa="{{$e->id}}" data-url=""><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    document.getElementById('addEmpresa').addEventListener('click', () => {
        openComponentModal('{{route('business.create')}}', 'Criar Empresa', 'xl', 'empresas.criarForm')
    })

    const editEmpresas = document.querySelectorAll('.editEmpresa');
    editEmpresas.forEach(editEmpresa => {
        editEmpresa.addEventListener('click', function(e){
            let url = this.getAttribute('data-url');
            let id = this.getAttribute('data-empresa');
            openComponentModal(url, 'Editar Empresa', 'xl', 'empresas.criarForm')
        })
    });
</script>
@endsection
