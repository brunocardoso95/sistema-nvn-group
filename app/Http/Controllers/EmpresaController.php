<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresas;
use App\Models\Historico;

class EmpresaController extends Controller
{
    //
    public function index(Request $req) {
        $data = [];
        $data['empresas'] = Empresas::all();
        return view('business.index', $data);
    }

    public function create(Request $req, $id = 0) {
        try{
            $data = [];
            if($id != 0) $empresa = Empresas::find($id);
            if($id == 0) $empresa = new Empresas();
            $data['e'] = $empresa;

            if($req->isMethod('POST')){
                $empresa->fill($req->all());
                $empresa->cnpj = preg_replace("/[^0-9]/", "", $req->input('cnpj'));
                $empresa->cpf_razaosocial = preg_replace("/[^0-9]/", "", $req->input('cpf_razaosocial'));
                $empresa->celular = preg_replace("/[^0-9]/", "", $req->input('celular'));
                $empresa->telefone = preg_replace("/[^0-9]/", "", $req->input('telefone'));
                $empresa->cep = preg_replace("/[^0-9]/", "", $req->input('cep'));
                $empresa->save();

                $req->session()->flash('success', 'Empresa criada com sucesso');
                return redirect()->route('business');
            }
        }catch(\Exception $e){
            \Log::error('Erro ao criar empresa [method: EmpresaController@create]: ', [$e->getMessage()]);
        }

        return view('business.create', $data);
    }

    public function apiBusiness(Request $req, $id = 0) {
        $u = auth()->user();

        $route = 'api.apiBusiness';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if($id != 0){
                $empresas = Empresas::where('id', $id)->first(['id','cnpj','razaosocial','fantasia','conexao','email','cpf_razaosocial','responsavel','celular','telefone','cep','logradouro','numero','complemento','bairro','cidade','uf','observacao','vencimento']);
            }else{
                $empresas = Empresas::all(['id','cnpj','razaosocial','fantasia','conexao','email','cpf_razaosocial','responsavel','celular','telefone','cep','logradouro','numero','complemento','bairro','cidade','uf','observacao','vencimento']);
            }
            if($empresas != null){
                $response = [
                    'error' => false,
                    'empresas' => $empresas
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 200);
            }else{
                $response = [
                    'error' => true,
                    'empresas' => 'Empresa não encontrada'
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 500);
            }
        }catch(\Error $e){
            \Log::error('[method: EmpresaController@apiBusiness]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'mensagem' => 'Erro na API'
                ]
            ], 500);
        }
    }

    public function apiBusinessCreate(Request $req, $id = 0) {
        $u = auth()->user();

        $route = 'api.apiBusinessCreate';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if($id != 0) $empresa = Empresas::find($id);
            if($id == 0) $empresa = new Empresas();

            $empDuplicatada = Empresas::where('cnpj', preg_replace("/[^0-9]/", "", $req->input('cnpj')))->first();
            if($empDuplicatada == null){
                $empresa->fill($req->all());
                $empresa->cnpj = preg_replace("/[^0-9]/", "", $req->input('cnpj'));
                $empresa->cpf_razaosocial = preg_replace("/[^0-9]/", "", $req->input('cpf_razaosocial'));
                $empresa->celular = preg_replace("/[^0-9]/", "", $req->input('celular'));
                $empresa->telefone = preg_replace("/[^0-9]/", "", $req->input('telefone'));
                $empresa->cep = preg_replace("/[^0-9]/", "", $req->input('cep'));
                if($empresa->save()){
                    $response = [
                        'error' => false,
                        'mensagem' => 'Empresa criada com sucesso.',
                        'idEmpresa' => $empresa->id
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response, 200);
                }else{
                    $response = [
                        'error' => true,
                        'mensagem' => 'Não consegui salvar essa Empresa.'
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response, 500);
                }
            }else{
                $response = [
                    'error' => true,
                    'mensagem' => 'Esse CNPJ já está cadastrada no Sistema.'
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 500);
            }
        }catch(\Exception $e){
            \Log::error('Erro ao criar empresa [method: EmpresaController@create]: ', [$e->getMessage()]);
            return response()->json(['error'=>true, 'mensagem' => 'Houve um erro da API, tente novamente mais tarde.'], 500);
        }
    }

    public function apiBusinessDelete(Request $req, $id = 0) {
        $u = auth()->user();

        $route = 'api.apiBusinessDelete';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if($id != 0) {
                $emp = Empresas::find($id);
                $emp->status = -1;
                if($emp->save()) {
                    $response = [
                        'error' => false,
                        'empresas' => 'Empresa deletada com sucesso.'
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response, 200);
                }else{
                    $response = [
                        'error' => true,
                        'mensagem' => 'Houve um erro ao deletar essa empresa.'
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response, 500);
                }
            }else{
                $response = [
                    'error' => true,
                    'mensagem' => 'Não encontramos a empresa informada.'
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 404);
            }
        }catch(\Exception $e){
            \Log::error('Erro ao criar empresa [method: EmpresaController@create]: ', [$e->getMessage()]);
            return response()->json(['error'=>true, 'mensagem' => 'Houve um erro da API, tente novamente mais tarde.'], 500);
        }
    }
}
