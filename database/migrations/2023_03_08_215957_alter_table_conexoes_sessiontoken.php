<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('conexoes', function (Blueprint $table) {
            $table->string('session')->after('webhook')->nullable();
            $table->string('token')->after('session')->nullable();
            $table->string('chatwootEnable')->after('token')->default(false);
            $table->string('chatwootUrl')->after('chatwootEnable')->default('https://app.chatwoot.com');
            $table->string('chatwootToken')->after('chatwootUrl')->nullable();
            $table->string('chatwootAccount')->after('chatwootToken')->nullable();
            $table->string('chatwootInbox')->after('chatwootAccount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('conexoes', function (Blueprint $table) {
            $table->dropColumn('session');
            $table->dropColumn('token');
            $table->dropColumn('chatwootEnable');
            $table->dropColumn('chatwootUrl');
            $table->dropColumn('chatwootToken');
            $table->dropColumn('chatwootAccount');
            $table->dropColumn('chatwootInbox');
        });
    }
};
