<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('config_disparo');
        Schema::dropIfExists('contatos');
        Schema::dropIfExists('mensagens');
        Schema::dropIfExists('protocolo');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('config_disparo', function (Blueprint $table) {
            $table->id();
            $table->string('file');
            $table->integer('telefone');
            $table->integer('identificador')->nullable();
            $table->integer('nome')->nullable();
            $table->text('mensagem')->nullable();
            $table->integer('status');
            $table->unsignedBigInteger('conexoes_id');
            $table->timestamps();

            $table->index('telefone');
            $table->index('status');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
        });
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('remoteJid', 30);
            $table->string('pushName')->nullable();
            $table->bigInteger('numero')->nullable();
            $table->string('nome')->nullable();
            $table->string('presences')->nullable();
            $table->string('ultimamensagem', 30)->nullable();
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('conexoes_id');
            $table->timestamps();

            $table->index('numero');
            $table->index('remoteJid');
            $table->index('conexoes_id');
            $table->index('ultimamensagem');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
        });
        Schema::create('mensagens', function (Blueprint $table) {
            $table->id();
            $table->string('keyId');
            $table->string('remoteJid');
            $table->boolean('fromMe');
            $table->string('text')->nullable();
            $table->unsignedBigInteger('messageTimestamp');
            $table->string('status')->nullable();
            $table->unsignedBigInteger('conexoes_id');
            $table->unsignedBigInteger('protocolo_id')->nullable();
            $table->timestamps();

            $table->index('keyId');
            $table->index('remoteJid');
            $table->index('status');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
            $table->foreign('protocolo_id')->references('id')->on('protocolo');
        });
        Schema::create('protocolo', function (Blueprint $table) {
            $table->id();
            $table->string('protocolo');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->index('protocolo');

            $table->foreign('users_id')->references('id')->on('users');
        });
    }
};
