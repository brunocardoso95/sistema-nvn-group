<?php

namespace App\ApiClient;

class WhatsAppApi extends ApiClient {
    protected $apiClient;
    public $token;
    public $urlWhatsApp;

    public function __construct() {
        $this->token = env('TOKEN_WHATSAPP');
        $this->urlWhatsApp = env('URL_WHATSAPP');
    }

    /* Uso geral */
    public function status() {
        $dados = $this->get("$this->urlWhatsApp/healthz");
        return $dados;
    }

    public function startAll() {
        $dados = $this->post("$this->urlWhatsApp/api/$this->token/start-all");
        return $dados;
    }

    public function generateToken($sessao) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/$this->token/generate-token");
        return $dados;
    }

    /* Inicio de Sessão - Auth */
    public function startSession($sessao, $tokenSessao, $enableChatwoot = false, $urlChatwoot = '', $tokenChatwoot = '', $inboxChatwoot = 0, $accountChatwoot = 0) {
        $body["webhook"] = "https://sistemas.nvngroup.com.br/api/webhook/'.$sessao.'";
        $body["waitQrCode"] = true;
        $body["chatWoot"]["enable"] = $enableChatwoot;
        $body["chatWoot"]["baseURL"] = $urlChatwoot;
        $body["chatWoot"]["token"] = $tokenChatwoot;
        $body["chatWoot"]["inbox_id"] = $inboxChatwoot;
        $body["chatWoot"]["account_id"] = $accountChatwoot;

        $dados = $this->post("$this->urlWhatsApp/api/$sessao/start-session", $tokenSessao, $body);
        return $dados;
    }

    public function statusSession($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/status-session", $tokenSessao);
        return $dados;
    }

    public function qrCodeSession($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/qrcode-session", $tokenSessao);
        return $dados;
    }

    public function checkConnectionSession($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/check-connection-session", $tokenSessao);
        return $dados;
    }

    public function closeSession($sessao, $tokenSessao) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/close-session", $tokenSessao);
        return $dados;
    }

    public function logoutSession($sessao, $tokenSessao) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/logout-session", $tokenSessao);
        return $dados;
    }

    /* Envio de Mensagens */
    public function sendFile($sessao, $tokenSessao, $telefone, $path, $caption = null, $nomeArquivo = null, $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true, $group);
        $body["phone"] = $telefone;
        $body['caption'] = $caption;
        $body["filename"] = $nomeArquivo;
        $body["path"] = $path;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-file", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendFileBase64($sessao, $tokenSessao, $telefone, $base64, $nomeArquivo, $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true, $group);
        $body["phone"] = $telefone;
        $body["filename"] = $nomeArquivo;
        $body["base64"] = $base64;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-file-base64", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendImage($sessao, $tokenSessao, $telefone, $base64, $mensagem = "", $nomeArquivo = "Arquivo", $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true, $group);
        sleep(1);
        $body["phone"] = $telefone;
        $body["filename"] = $nomeArquivo;
        $body["message"] = $mensagem;
        $body["base64"] = $base64;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-image", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendVoice($sessao, $tokenSessao, $telefone, $base64, $group = false) {
        $this->setRecording($sessao, $tokenSessao, $telefone, true, $group);
        sleep(1);
        $body["phone"] = $telefone;
        $body["base64Ptt"] = $base64;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-voice-base64", $tokenSessao, $body);
        $this->setRecording($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendReply($sessao, $tokenSessao, $telefone, $mensagem, $idMensagem, $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true, $group);
        sleep(1);
        $body["phone"] = $telefone;
        $body["message"] = $mensagem;
        $body["messageId"] = $idMensagem;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-reply", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendMessage($sessao, $tokenSessao, $telefone, $mensagem, $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true, $group);
        sleep(1);
        $body["phone"] = $telefone;
        $body["message"] = $mensagem;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-message", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false, $group);
        return $dados;
    }

    public function sendButtons($sessao, $tokenSessao, $telefone, $botoes, $usarTemplate = true, $titulo = null, $mensagem = null, $rodape = null) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["options"]["useTemplateButtons"] = $usarTemplate;
        $body["options"]["buttons"] = $botoes;
        $body["title"] = $titulo;
        $body["message"] = $mensagem;
        $body["footer"] = $rodape;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-buttons", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function forwardMessage($sessao, $tokenSessao, $telefone, $idMensagem) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["messageId"] = $idMensagem;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/forwardMessages", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function contactVcard($sessao, $tokenSessao, $telefone, $idContato, $nomeContato = null, $group = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["contactsId"] = $idContato;
        $body["name"] = $nomeContato;
        $body["group"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/contact-vcard", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function sendLinkPreview($sessao, $tokenSessao, $telefone, $url, $mensagem = null) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["caption"] = $mensagem;
        $body["url"] = $url;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-link-preview", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function sendLocation($sessao, $tokenSessao, $telefone, $lat = 0, $lng = 0, $titulo, $endereco = '') {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["title"] = $titulo;
        $body["address"] = $endereco;
        $body["lat"] = $lat;
        $body["lng"] = $lng;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-location", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function sendSticker($sessao, $tokenSessao, $telefone, $arquivo, $isGroup = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["path"] = $arquivo;
        $body["isGroup"] = $isGroup;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-sticker", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    public function sendPollMessage($sessao, $tokenSessao, $telefone, $nome, $escolhas = [], $selecionaveis = 1, $isGroup = false) {
        $this->setTyping($sessao, $tokenSessao, $telefone, true);
        sleep(1);
        $body["phone"] = $telefone;
        $body["name"] = $nome;
        $body["choices"] = $escolhas;
        $body["options"]["selectableCount"] = $selecionaveis;
        $body["isGroup"] = $isGroup;
        //dd(json_encode($body));
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-poll-message", $tokenSessao, $body);
        $this->setTyping($sessao, $tokenSessao, $telefone, false);
        return $dados;
    }

    /* Contatos*/
    public function subscribePresence($sessao, $tokenSessao, $telefone, $grupo = false, $all = true) {
        $body['phone'] = $telefone;
        $body['isGroup'] = $grupo;
        $body['all'] = $all;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/subscribe-presence", $tokenSessao, $body);
        return $dados;
    }

    public function checkNumberStatus($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/check-number-status/$telefone", $tokenSessao);
        return $dados;
    }

    public function allContacts($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-contacts", $tokenSessao);
        return $dados;
    }

    public function getContact($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/contact/$telefone", $tokenSessao);
        return $dados;
    }

    //Deprecated
    public function getNumberProfile($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/profile/$telefone", $tokenSessao);
        return $dados;
    }

    public function getProfilePicture($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/profile-pic/$telefone", $tokenSessao);
        return $dados;
    }

    public function getProfileStatus($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/profile-status/$telefone", $tokenSessao);
        return $dados;
    }

    /* Phone Status */
    public function hostDevice($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/host-device", $tokenSessao);
        return $dados;
    }

    /* Lista de Bloqueios */
    public function blockList($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/blocklist", $tokenSessao);
        return $dados;
    }

    public function blockContact($sessao, $tokenSessao, $telefone) {
        $body["phone"] = $telefone;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/block-contact", $tokenSessao, $body);
        return $dados;
    }

    public function unblockContact($sessao, $tokenSessao, $telefone) {
        $body["phone"] = $telefone;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/unblock-contact", $tokenSessao, $body);
        return $dados;
    }

    /* Chat */
    public function setTyping($sessao, $tokenSessao, $telefone, $value = false, $group = false) {
        $body["phone"] = $telefone;
        $body["value"] = $value;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/typing", $tokenSessao, $body);
        return $dados;
    }

    public function setRecording($sessao, $tokenSessao, $telefone, $value = false, $group = false) {
        $body["phone"] = $telefone;
        $body["value"] = $value;
        $body["isGroup"] = $group;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/recording", $tokenSessao, $body);
        return $dados;
    }

    public function changeUsername($sessao, $tokenSessao, $nome) {
        $body["name"] = $nome;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/change-username", $tokenSessao, $body);
        return $dados;
    }

    public function changeProfileImage($sessao, $tokenSessao, $urlImagem) {
        $body["path"] = $urlImagem;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/change-profile-image", $tokenSessao, $body);
        return $dados;
    }

    public function profileStatus($sessao, $tokenSessao, $status) {
        $body["status"] = $status;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/profile-status", $tokenSessao, $body);
        return $dados;
    }

    //Deprecated
    public function allChat($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-chats", $tokenSessao);
        return $dados;
    }

    //Deprecated
    public function allChatWithMessage($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-chats-with-messages", $tokenSessao);
        return $dados;
    }

    public function allChatArchived($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-chats-archived", $tokenSessao);
        return $dados;
    }

    public function listChats($sessao, $tokenSessao, $idContato = "", $count = 50, $withLabels = false, $onlyGroups = false, $onlyUsers = false, $onlyWithUnreadMessage = false, $direction = "after") {
        $body['id'] = $idContato;
        $body['count'] = $count;
        $body['direction'] = $direction;
        $body['onlyGroups'] = $onlyGroups;
        $body['onlyUsers'] = $onlyUsers;
        $body['onlyWithUnreadMessage'] = $onlyWithUnreadMessage;
        $body['withLabels'] = $withLabels;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/list-chats", $tokenSessao, $body);
        return $dados;
    }

    public function chatById($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/chat-by-id/$telefone", $tokenSessao);
        return $dados;
    }

    public function chatIsOnline($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/chat-is-online/$telefone", $tokenSessao);
        return $dados;
    }

    public function getMessages($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/get-messages/$telefone", $tokenSessao);
        return $dados;
    }

    public function getMediaByMessage($sessao, $tokenSessao, $msgId) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/get-media-by-message/$msgId", $tokenSessao);
        return $dados;
    }

    public function allNewMessages($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-new-messages", $tokenSessao);
        return $dados;
    }

    public function unreadMessages($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/unread-messages", $tokenSessao);
        return $dados;
    }

    public function allUnreadMessages($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-unread-messages", $tokenSessao);
        return $dados;
    }

    public function lastSeen($sessao, $tokenSessao, $telefone) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/last-seen/$telefone", $tokenSessao);
        return $dados;
    }

    public function listMutes($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/list-mutes", $tokenSessao);
        return $dados;
    }

    public function reactions($sessao, $tokenSessao, $msgId) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/reactions/$msgId", $tokenSessao);
        return $dados;
    }

    public function votes($sessao, $tokenSessao, $msgId) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/votes/$msgId", $tokenSessao);
        return $dados;
    }

    public function archiveChat($sessao, $tokenSessao, $telefone, $grupo = false) {
        $body['phone'] = $telefone;
        $body['isGroup'] = $grupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/archive-chat", $tokenSessao, $body);
        return $dados;
    }

    public function archiveAllChats($sessao, $tokenSessao) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/archive-all-chats", $tokenSessao);
        return $dados;
    }

    public function clearChat($sessao, $tokenSessao, $telefone) {
        $body['phone'] = $telefone;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/clear-chat", $tokenSessao, $body);
        return $dados;
    }

    public function deleteChat($sessao, $tokenSessao, $telefone) {
        $body['phone'] = $telefone;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/delete-chat", $tokenSessao, $body);
        return $dados;
    }

    public function deleteAllChats($sessao, $tokenSessao) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/delete-all-chats", $tokenSessao);
        return $dados;
    }

    public function deleteMessage($sessao, $tokenSessao, $telefone, $msgId, $grupo = false) {
        $body['phone'] = $telefone;
        $body['messageId'] = $msgId;
        $body['isGroup'] = $grupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/delete-chat", $tokenSessao, $body);
        return $dados;
    }

    public function reactMessage($sessao, $tokenSessao, $msgId, $reaction) {
        $body['msgId'] = $msgId;
        $body['isGroup'] = $reaction;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/react-mssage", $tokenSessao, $body);
        return $dados;
    }

    public function markUseen($sessao, $tokenSessao, $telefone, $grupo = false) {
        $body['phone'] = $telefone;
        $body['isGroup'] = $grupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/mark-useen", $tokenSessao, $body);
        return $dados;
    }

    public function sendSeen($sessao, $tokenSessao, $telefone) {
        $body['phone'] = $telefone;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/send-seen", $tokenSessao, $body);
        return $dados;
    }

    public function pinChat($sessao, $tokenSessao, $telefone, $msgId) {
        $body['phone'] = $telefone;
        $body['messageId'] = $msgId;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/pin-chat", $tokenSessao, $body);
        return $dados;
    }

    public function sendMute($sessao, $tokenSessao, $telefone, $tempo = 1, $tipo = 'hours') {
        $body['phone'] = $telefone;
        $body['time'] = $tempo;
        $body['type'] = $tipo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/pin-chat", $tokenSessao, $body);
        return $dados;
    }

    public function temporaryMessages($sessao, $tokenSessao, $telefone, $valor = true) {
        $body['phone'] = $telefone;
        $body['value'] = $valor;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/temporary-messages", $tokenSessao, $body);
        return $dados;
    }

    public function rejectCall($sessao, $tokenSessao, $idLigacao, $valor = true) {
        $body['callId'] = $idLigacao;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/reject-call", $tokenSessao, $body);
        return $dados;
    }

    /* Business */
    public function getAllLabels($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/get-all-labels", $tokenSessao);
        return $dados;
    }

    public function addNewLabel($sessao, $tokenSessao, $nome, $cor = "#fff") {
        $body['name'] = $nome;
        $body['options']['labelColor'] = $cor;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/add-new-label", $tokenSessao, $body);
        return $dados;
    }

    public function addOrRemoveLabel($sessao, $tokenSessao, $chatId, $labelId, $tipo) {
        $body['chatIds'] = [$chatId];
        $body['options'] = ['labelId' => $labelId, 'type'=>$tipo];
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/add-or-remove-label", $tokenSessao, $body);
        return $dados;
    }

    /* Grupo */
    public function createGroup($sessao, $tokenSessao, $nome, $participantes = "") {
        $body['name'] = $nome;
        $body['participants'] = $participantes;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/create-group", $tokenSessao, $body);
        return $dados;
    }

    public function joinCode($sessao, $tokenSessao, $link) {
        $body['inviteCode'] = $link;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/join-code", $tokenSessao, $body);
        return $dados;
    }

    public function groupInfoFromInviteLink($sessao, $tokenSessao, $link) {
        $body['invitecode'] = $link;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/group-info-from-invite-link", $tokenSessao, $body);
        return $dados;
    }

    public function groupInviteLink($sessao, $tokenSessao, $idGrupo) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-invite-link/$idGrupo", $tokenSessao);
        return $dados;
    }

    public function groupRevokeLink($sessao, $tokenSessao, $idGrupo) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-revoke-link/$idGrupo", $tokenSessao);
        return $dados;
    }

    public function groupMembers($sessao, $tokenSessao, $idGrupo) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-members/$idGrupo", $tokenSessao);
        return $dados;
    }

    public function groupMembersIds($sessao, $tokenSessao, $idGrupo) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-members-ids/$idGrupo", $tokenSessao);
        return $dados;
    }

    public function addParticipantGroup($sessao, $tokenSessao, $telefone, $idGrupo) {
        $body['phone'] = $telefone;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/add-participant-group", $tokenSessao, $body);
        return $dados;
    }

    public function promoteParticipantGroup($sessao, $tokenSessao, $telefone, $idGrupo) {
        $body['phone'] = $telefone;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/promote-participant-group", $tokenSessao, $body);
        return $dados;
    }

    public function demoteParticipantGroup($sessao, $tokenSessao, $telefone, $idGrupo) {
        $body['phone'] = $telefone;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/demote-participant-group", $tokenSessao, $body);
        return $dados;
    }

    public function removeParticipantGroup($sessao, $tokenSessao, $telefone, $idGrupo) {
        $body['phone'] = $telefone;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/remove-participant-group", $tokenSessao, $body);
        return $dados;
    }

    public function groupSubject($sessao, $tokenSessao, $titulo, $idGrupo) {
        $body['title'] = $titulo;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/group-subject", $tokenSessao, $body);
        return $dados;
    }

    public function groupDescription($sessao, $tokenSessao, $descricao, $idGrupo) {
        $body['description'] = $descricao;
        $body['groupId'] = $idGrupo;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/group-description", $tokenSessao, $body);
        return $dados;
    }

    public function groupProperty($sessao, $tokenSessao, $proprietario, $idGrupo, $valor = false) {
        $body['property'] = $proprietario;
        $body['groupId'] = $idGrupo;
        $body['value'] = $valor;
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/group-property", $tokenSessao, $body);
        return $dados;
    }

    public function allBroadcastList($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-broadcast-list", $tokenSessao);
        return $dados;
    }

    public function allGroups($sessao, $tokenSessao) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/all-groups", $tokenSessao);
        return $dados;
    }

    public function groupAdmins($sessao, $tokenSessao, $idGrupo) {
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-admins/$idGrupo", $tokenSessao);
        return $dados;
    }

    public function leaveGroup($sessao, $tokenSessao, $idGrupo) {
        $body['groupId'] = $idGrupo;
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/leave-group", $tokenSessao, $body);
        return $dados;
    }

    public function messagesAdminsOnly($sessao, $tokenSessao, $idGrupo, $valor = false) {
        $body['groupId'] = $idGrupo;
        $body['value'] = $valor;
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/messages-admins-only", $tokenSessao, $body);
        return $dados;
    }

    public function groupPic($sessao, $tokenSessao, $idGrupo, $link) {
        $body['phone'] = "$idGrupo@g.us";
        $body['path'] = $link;
        $body['isGroup'] = true;
        $dados = $this->get("$this->urlWhatsApp/api/$sessao/group-pic", $tokenSessao, $body);
        return $dados;
    }

    /* Chatwoot */
    public function chatwoot($sessao, $req) {
        $dados = $this->post("$this->urlWhatsApp/api/$sessao/chatwoot", false, $req);
        return $dados;
    }

    /* Webhook */
    public function webhook($url, $dados){
        $dados = $this->post($url, false, $dados);
        return $dados;
    }
}
