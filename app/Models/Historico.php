<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    use HasFactory;
    public $table = "historico";

    protected $fillable = [
        'usuario_id',
        'route',
        'path',
        'method',
        'header',
        'cookie',
        'request',
        'response',
    ];
}
