@extends('layouts.system')

@section('content')
<div class="row g-3 mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    v1.1 (10/06/2023)
                </div>
                <div class="card-text">
                    <ul>
                        <li>Alteração do Endpoint [todos-chat], [todos-chat-com-mensagem] e [todos-grupos] para [listar-chats].</li>
                        <li>Introdução a Comunidade do WhatsApp.</li>
                        <li>Melhorias na parte de Etiquetas (Label) para WhatsApp Business.</li>
                        <li>Introdução do Catalogo do WhatsApp Business.</li>
                        <li>Enviar botões foi descontinuado.</li>
                        <li>Inclusão do Endpoint para Chats Arquivados</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    v1 (01/04/2023)
                </div>
                <div class="card-text">
                    <ul>
                        <li>Criação da API</li>
                        <li>Organização de Endpoint</li>
                        <li>Criação de Webhook autônomo</li>
                        <li>Integração direta para Chatwoot</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
