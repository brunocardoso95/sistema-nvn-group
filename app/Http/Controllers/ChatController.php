<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiClient\WhatsAppApi;
use App\Models\Historico;

class ChatController extends Controller
{
    //
    public function blockList(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.blockContact';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $dados = $whats->blockList($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@blockList]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function blockContact(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.blockContact';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 400);
            }

            $dados = $whats->blockContact($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            if($dados->status == 'success'){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Contato bloqueado com sucesso',
                    ]
                ];
                $status = 200;
            }else{
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Houve um erro ao processar o bloqueio desse telefone',
                    ]
                ];
                $status = 401;
            }
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@blockContact]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function unblockContact(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.unblockContact';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 400);
            }

            $dados = $whats->unblockContact($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            if($dados->status == 'success'){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Contato desbloqueado com sucesso',
                    ]
                ];
                $status = 200;
            }else{
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Houve um erro ao processar o desbloqueio desse telefone',
                    ]
                ];
                $status = 401;
            }
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@unblockContact]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function changeUsername(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.changeUsername';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $nome = $req->input('nome', false);
            if(!$nome){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {nome} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->changeUsername($req['con']->session, $req['con']->token, $nome);
            $dados = json_decode($dados);

            if($dados->status == 'success'){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Nome alterado com sucesso',
                    ]
                ];
                $status = 200;
            }else{
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Houve um erro ao processar o desbloqueio desse telefone',
                    ]
                ];
                $status = 400;
            }
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@changeUsername]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function changeProfileImage(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.changeProfileImage';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $base64 = $req->input('base64', false);
            if(!$base64){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {base64} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->changeProfileImage($req['con']->session, $req['con']->token, $base64);
            $dados = json_decode($dados);

            if($dados->status == 'success'){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Foto alterado com sucesso',
                    ]
                ];
                $status = 200;
            }else{
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Houve um erro ao processar o desbloqueio desse telefone',
                    ]
                ];
                $status = 400;
            }
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@changeProfileImage]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function profileStatus(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.profileStatus';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $status = $req->input('status', false);
            if(!$status){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {status} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->profileStatus($req['con']->session, $req['con']->token, $status);
            $dados = json_decode($dados);

            if($dados->status == 'success'){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Status alterado com sucesso',
                    ]
                ];
                $status = 200;
            }else{
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Houve um erro ao processar o desbloqueio desse telefone',
                    ]
                ];
                $status = 400;
            }
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@profileStatus]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allChat(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.allChat';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $dados = $whats->allChat($req['con']->session, $req['con']->token);
            $chats = json_decode($dados);

            $response = [
                'error' => false,
                'alert' => 'Deprecated: This function [todos-chat] is deprecated in favor of the [listar-chats] function. Please update your code accordingly',
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@allChat]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allChatWithMessage(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.allChatWithMessage';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allChatWithMessage($req['con']->session, $req['con']->token);
            $chats = json_decode($dados);
            $response = [
                'error' => false,
                'alert' => 'Deprecated: This function [todos-chat-com-mensagem] is deprecated in favor of the [listar-chats] function. Please update your code accordingly',
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@allChatWithMessage]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allChatArchived(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.allChatArchived';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allChatArchived($req['con']->session, $req['con']->token);
            $chats = json_decode($dados);
            $response = [
                'error' => false,
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@allChatArchived]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function listChats(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.listChats';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', '');
            $contador = $req->input('contador', 50);
            $withLabels = $req->input('label', false);
            $onlyGroups = $req->input('grupo', false);
            $onlyUsers = $req->input('usuarios', false);
            $onlyWithUnreadMessage = $req->input('novasMensagens', false);

            $dados = $whats->listChats($req['con']->session, $req['con']->token, $telefone, $contador, $withLabels, $onlyGroups, $onlyUsers, $onlyWithUnreadMessage);
            $chats = json_decode($dados);
            $response = [
                'error' => false,
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@listChats]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function chatById(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.chatById';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->chatById($req['con']->session, $req['con']->token, $telefone);
            $chats = json_decode($dados);

            $response = [
                'error' => false,
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@chatById]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function chatIsOnline(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.chatIsOnline';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->chatIsOnline($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'status' =>  $dados->response,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@chatIsOnline]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function getMessages(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.getMessages';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            $tipo = $req->input('tipo', "");
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->getMessages($req['con']->session, $req['con']->token, $telefone);
            $msgs = json_decode($dados);
            //$msgs = [];

            $response = [
                'error' => false,
                'msgs' => $msgs,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@getMessages]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function getMediaByMessage(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.getMediaByMessage';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $idmensagem = $req->input('idmensagem', false);
            if(!$idmensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idmensagem} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->getMediaByMessage($req['con']->session, $req['con']->token, $idmensagem);
            $dados = json_decode($dados);
            //return count($response);
            $chats = [];

            $response = [
                'error' => false,
                'dados' => $dados,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@getMediaByMessage]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allNewMessages(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.allNewMessages';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allNewMessages($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);
            $response = $dados->response;
            //return count($response);
            $chats = [];

            $response = [
                'error' => false,
                'dados' => $dados,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@allNewMessages]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function unreadMessages(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.unreadMessages';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->unreadMessages($req['con']->session, $req['con']->token);
            $chats = json_decode($dados);
            /*$response = $dados->response;
            //return count($response);
            $chats = [];*/

            $response = [
                'error' => false,
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@unreadMessages]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allUnreadMessages(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.allUnreadMessages';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allUnreadMessages($req['con']->session, $req['con']->token);
            $chats = json_decode($dados);
            /*$response = $dados->response;
            //return count($response);
            $chats = [];*/

            $response = [
                'error' => false,
                'chats' => $chats,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@allUnreadMessages]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function lastSeen(Request $req, $telefone = false) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.lastSeen';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 400);
            }
            $dados = $whats->lastSeen($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);
            /*$response = $dados->response;
            //return count($response);
            $chats = [];*/

            $response = [
                'error' => false,
                'dados' => $dados,
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ChatController@lastSeen]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }
}
