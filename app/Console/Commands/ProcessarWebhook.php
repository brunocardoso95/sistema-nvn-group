<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Webhook;
use App\ApiClient\ApiClient;

class ProcessarWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:processar-webhook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dados = Webhook::where('enviado', 0)->get();
        foreach($dados as $d) {
            $http = new ApiClient();
            $response = $http->post($d->url, false, json_decode($d->response));
            if($response->successful()){
                $webhook = Webhook::find($d->id);
                $webhook->enviado = true;
                $webhook->save();
            }
        }
        return Command::SUCCESS;
    }
}
