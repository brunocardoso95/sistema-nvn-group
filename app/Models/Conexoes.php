<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conexoes extends Model
{
    use HasFactory;

    const OFFLINE = 0;
    const PENDENTE = 1;
    const QRCODE = 2;
    const ONLINE = 3;

    public $timestamps = true;
    public $incrementing = true;

    protected $fillable = [
        'nome',
        'status',
        'qrcode',
        'telefone',
        'webhook',
        'webhookUrl',
        'session',
        'token',
        'chatwootEnable',
        'chatwootUrl',
        'chatwootToken',
        'chatwootAccount',
        'chatwootInbox',
        'time',
        'empresa_id'
    ];

    public function getStatus() {
        switch($this->status){
            case 0:
                return 'Offline';
                break;
            case 1:
                return 'Pendente';
                break;
            case 2:
                return 'QrCode';
                break;
            case 3:
                return 'Online';
                break;
        }
    }
}
