<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Empresas extends Model
{
    use HasFactory;

    public $timestamps = true;
    public $incrementing = true;

    protected $fillable = [
        'cnpj',
        'razaosocial',
        'fantasia',
        'status',
        'conexao',
        'email',
        'cpf_razaosocial',
        'responsavel',
        'celular',
        'telefone',
        'logradouro',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'logo',
        'observacao',
        'vencimento',
    ];

    public function vencimentoDate() {
        $data = Carbon::createFromFormat('Y-m-d', $this->vencimento);
        return  $data->format('d/m/Y');
    }

    public function maskCnpj() {
        $maskared = '';
        $val = $this->cnpj;
        $mask = "##.###.###/####-##";
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $maskared .= $mask[$i];
                }
            }
        }

        return $maskared;
    }

    public function maskCpf() {
        $maskared = '';
        $val = $this->cpf_razaosocial;
        $mask = "###.###.###-##";
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $maskared .= $mask[$i];
                }
            }
        }

        return $maskared;
    }

    public function maskTel() {
        $maskared = '';
        $val = $this->telefone;
        $mask = "(##) ####-####";
        $k = 0;
        if($val != ''){
            for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
                if ($mask[$i] == '#') {
                    if (isset($val[$k])) {
                        $maskared .= $val[$k++];
                    }
                } else {
                    if (isset($mask[$i])) {
                        $maskared .= $mask[$i];
                    }
                }
            }
        }

        return $maskared;
    }

    public function maskCel() {
        $maskared = '';
        $val = $this->celular;
        $mask = "(##) #####-####";
        $k = 0;
        if($val != ''){
            for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
                if ($mask[$i] == '#') {
                    if (isset($val[$k])) {
                        $maskared .= $val[$k++];
                    }
                } else {
                    if (isset($mask[$i])) {
                        $maskared .= $mask[$i];
                    }
                }
            }
        }
        return $maskared;
    }
}
