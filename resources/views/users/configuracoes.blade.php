@extends('layouts.system')

@section('content')
<input type="hidden" id="idUsuario" value="{{$usuario->id}}">
<div class="row">
    <div class="col-12">
      <div class="card mb-3 btn-reveal-trigger">
        <div class="card-header position-relative min-vh-25 mb-8">
          <div class="cover-image">
            <div class="bg-holder rounded-3 rounded-bottom-0" style="background-image:url({{asset('assets/img/generic/4.jpg')}});"></div>
          </div>
          <div class="avatar avatar-5xl avatar-profile shadow-sm img-thumbnail rounded-circle">
            <div class="h-100 w-100 rounded-circle overflow-hidden position-relative">
                <img src="{{asset('assets/img/team/2.jpg')}}" width="200" alt="" data-dz-thumbnail="data-dz-thumbnail">
                <input class="d-none" id="profile-image" type="file">
                <label class="mb-0 overlay-icon d-flex flex-center" for="profile-image">
                    <span class="bg-holder overlay overlay-0"></span>
                    <span class="z-index-1 text-white dark__text-white text-center fs--1">
                        <span class="fas fa-camera"></span>
                        <span class="d-block">Upload de Foto</span>
                    </span>
                </label>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row g-0">
    <div class="col-lg-8 pe-lg-2">
        <div class="card mb-3">
            <div class="card-header">
                <h5 class="mb-0">Configuração de Perfil</h5>
            </div>
            <div class="card-body bg-light">
                <form class="row g-3">
                    <div class="col-lg-6">
                        <div class="form-floating">
                            <input class="form-control" id="name" placeholder="Nome" type="text" value="{{$usuario->name}}">
                            <label class="form-label" for="name">Nome</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-floating">
                            <input class="form-control" id="lastname" placeholder="Sobrenome" type="text" value="{{$usuario->lastname}}">
                            <label class="form-label" for="lastname">Sobrenome</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-floating">
                            <input class="form-control" id="email" placeholder="Email" type="email" value="{{$usuario->email}}">
                            <label class="form-label" for="email">Email</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-floating">
                            <input class="form-control" id="phone" placeholder="Telefone" type="text" value="{{$usuario->phone}}">
                            <label class="form-label" for="phone">Telefone</label>
                        </div>
                    </div>
                    <div class="col-12 d-flex justify-content-end"><button class="btn btn-primary" type="submit">Atualizar </button></div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4 ps-lg-2">
    <div class="sticky-sidebar">
        <div class="card mb-3">
            <div class="card-header">
                <h5 class="mb-0">Atualizar senha</h5>
            </div>
            <div class="card-body bg-light">
                <form>
                    <input type="hidden" name="_token" id="tokenPassword" value="{{ csrf_token() }}">
                    <div class="mb-3">
                        <div class="form-floating">
                            <input class="form-control" id="old-password" type="password" placeholder="***">
                            <label class="form-label" for="old-password">Senha antiga</label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input class="form-control" id="new-password" type="password" placeholder="***">
                            <label class="form-label" for="new-password">Nova senha</label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input class="form-control" id="confirm-password" type="password" placeholder="***">
                            <label class="form-label" for="confirm-password">Confirmar a nova senha</label>
                        </div>
                    </div>
                    <button class="btn btn-primary d-block w-100" id="changepassword" type="button">Atualizar a senha</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Zona de Perigo <i class="fa-solid fa-triangle-exclamation ms-1 text-danger"></i></h5>
            </div>
            <div class="card-body bg-light">
                @if($usuario->isAdmin())
                <h5 class="fs-0">Transferir Usuário</h5>
                <p class="fs--1">Transferir esse usuário para outra empresa.</p>
                <a class="btn btn-falcon-warning d-block" href="#!">Transferir</a>
                <div class="border-bottom border-dashed my-4"></div>
                @endif
                <h5 class="fs-0">Excluir a conta</h5>
                <p class="fs--1">Tenha certeza que quer excluir essa conta, esse procedimento não tem retorno.</p>
                <a class="btn btn-falcon-danger d-block" href="#!">Excluir conta</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    document.getElementById('changepassword').addEventListener('click', async () => {
        const idUsuario =       document.getElementById('idUsuario').value
        const token =           document.getElementById('tokenPassword').value
        const senhaAntiga =     document.getElementById('old-password').value
        const novaSenha =       document.getElementById('new-password').value
        const confirmarSenha =  document.getElementById('confirm-password').value

        if(senhaAntiga != '' || novaSenha != '' || confirmarSenha != ''){
            if(novaSenha === confirmarSenha){
                var trocaDeSenha = new XMLHttpRequest();
                trocaDeSenha.open('PUT', '{{route('users.trocaSenha')}}', true);
                trocaDeSenha.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                trocaDeSenha.send("_token="+token+"&id="+idUsuario+"&antiga="+senhaAntiga+"&novo="+novaSenha+"&confirma="+confirmarSenha);
                trocaDeSenha.onreadystatechange = function() {
                    if (trocaDeSenha.readyState == 4) {
                        const dados = JSON.parse(trocaDeSenha.responseText);
                        const data = dados.data
                        document.getElementById('toastTitle').innerHTML = data.title
                        document.getElementById('toastText').innerHTML  = data.message
                        const toastLiveExample = document.getElementById('liveToast')
                        const toast = new bootstrap.Toast(toastLiveExample)
                        toast.show()
                    }
                }
            }else{
                document.getElementById('toastTitle').innerHTML = 'Atualizar senha'
                document.getElementById('toastText').innerHTML  = 'A senha nova é diferente da confirmação tem que ser iguais.'
                const toastLiveExample = document.getElementById('liveToast')
                const toast = new bootstrap.Toast(toastLiveExample)
                toast.show()
            }
        }else{
            document.getElementById('toastTitle').innerHTML = 'Atualizar senha'
            document.getElementById('toastText').innerHTML  = 'Os campos se encontram vazios'
            const toastLiveExample = document.getElementById('liveToast')
            const toast = new bootstrap.Toast(toastLiveExample)
            toast.show()
        }
    })
</script>
@endsection
