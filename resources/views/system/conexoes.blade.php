@extends('layouts.system')

@section('content')
<div class="container-fluid">
    <nav style="--falcon-breadcrumb-divider: '»';" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Conexões</li>
          <li class="breadcrumb-item"><a href="#" id="addConexao" class="me-1"><i class="align-middle fa-solid fa-add"></i></a></li>
        </ol>
    </nav>

    <div class="row">
    @foreach($conexoes as $c)
    <div class="col-lg-2">
        <div class="card overflow-hidden h-100 w-100" id="id_{{$c->id}}">
            <div class="bg-holder bg-card" style="background-image:url({{asset('assets/img/icons/spot-illustrations/corner-4.png')}});"></div>
            <div class="card-body position-relative">
                <h5 class="card-title" id="server_{{$c->id}}">{{$c->nome}}</h5>
                <p class="card-text">
                    <a class="btn btn-sm btn-primary btn-whatsapp" id="btn-whatsapp_{{$c->id}}" data-canal="{{$c->id}}">{{$c->getStatus()}}</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
    </div>
</div>
@endsection

@section('js')
<script>
    @foreach($conexoes as $c)
    var channel = Echo.channel('conexoes.{{$c->id}}');
    channel.listen('.status', function(data) {
        console.info(JSON.stringify(data));
    });
    @endforeach

    const whatsappBtn = document.querySelectorAll('.btn-whatsapp');
    whatsappBtn.forEach(wa => {
        wa.addEventListener('click', function(e){
            let id = this.getAttribute('data-canal');
            openComponentModal("{{route('conexoes.status')}}?id="+id, 'Status da Conexão', 'md')
        })
    });

    document.getElementById('addConexao').addEventListener('click', () => {
        openComponentModal('{{route('conexao.create')}}', 'Criar Conexão', 'xl', 'empresas.criarForm')
    })
</script>
@endsection
