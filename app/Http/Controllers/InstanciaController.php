<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiClient\WhatsAppApi;
use App\Models\Conexoes;
use App\Models\Empresas;
use App\Models\Historico;
use Log;

class InstanciaController extends Controller
{
    //

    public function init(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'conexao.init';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
		try{
			$conexoes = [];
			$con = Conexoes::where('empresa_id', $u->empresa_id)->where('status', '>=', 0)->get();
			foreach($con as $c){
                $nome = mb_strtolower($c->nome);
                $nome = str_replace(' ', '_', $nome);
                $dados = $whats->generateToken($c->id.'_'.$nome);
                $dados = json_decode($dados);
				//array_push($conexoes, $dados);

                $newC = Conexoes::find($c->id);
                $newC->session = $dados->session;
                $newC->token = $dados->token;
                $newC->save();
			}

            $con = Conexoes::where('empresa_id', $u->empresa_id)->where('status', '>=', 0)->get();
            foreach($con as $c){
                $dados = $whats->startSession($c->session, $c->token, $c->chatwootEnable, $c->chatwootUrl, $c->chatwootToken, $c->chatwootInbox, $c->chatwootAccount);
                $dados = json_decode($dados);
                $dados->version = 'v1';

                $newC = Conexoes::find($c->id);
                switch($dados->status){
                    case 'CLOSE':
                    $newC->status = 0;
                    $dados->status = "Conexão Fechada";
                    $dados->qrcode = null;
                    $dados->urlcode = null;
                    $newC->qrcode = $dados->qrcode;
                    break;
                    case 'INITIALIZING':
                    $newC->status = 1;
                    $dados->status = "Iniciando";
                    $dados->qrcode = null;
                    $dados->urlcode = null;
                    $newC->qrcode = $dados->qrcode;
                    break;
                    case 'QRCODE':
                    $newC->status = 2;
                    $dados->status = "Aguardando QRCode";
                    $newC->qrcode = $dados->qrcode;
                    break;
                    case 'CONNECTED':
                    $newC->status = 3;
                    $dados->status = "Conectado";
                    $dados->qrcode = null;
                    $dados->urlcode = null;
                    $newC->qrcode = $dados->qrcode;
                    break;
                }
                $newC->save();

				array_push($conexoes, $dados);
            }

            $response = [
                'error' => false,
                'conexoes' => $conexoes
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
		}catch(\Exception $e){
            Log::error('Erro na API [method: InstanciaController@init]: ', [$e->getMessage()]);
			return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
		}
    }

    public function create(Request $req) {
        $u = auth()->user();

        $route = 'conexao.create';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        $empresa = Empresas::find($u->empresa_id);
        $con = Conexoes::where('empresa_id', $u->empresa_id)->where('status', '>=', 0)->count();

        try{
            if($con >= $empresa->conexao){
                return response()->json([
                    'error' => true,
                    'data' => [
                        'conexoes' => 'Limite de conexão excedido',
                        'limite' => $empresa->conexao,
                        'quant_conexao' => $con
                    ]
                ], 401);
            }else{
                $conn = new Conexoes();
                $conn->nome = $req->input('nome');
                $conn->status = 0;
                $conn->empresa_id = $empresa->id;
                $conn->webhook = $req->input('webhook', false);
                $conn->webhookUrl = $req->input('webhookUrl', null);
                $conn->chatwootEnable = $req->input('chatwootEnable', null);
                $conn->chatwootUrl = $req->input('chatwootUrl', null);
                $conn->chatwootToken = $req->input('chatwootToken', null);
                $conn->chatwootInbox = $req->input('chatwootInbox', null);
                $conn->chatwootAccount = $req->input('chatwootAccount', null);

                $nome = mb_strtolower($conn->nome);
                $nome = str_replace(' ', '_', $nome);
                if($conn->save()){
                    $conn->session = $conn->id.'_'.$nome;
                    $conn->save();

                    $response = [
                        'error' => false,
                        'data' => [
                            'conexoes' => "A conexão $conn->id - $conn->nome foi criado com sucesso",
                            'limite' => $empresa->conexao,
                            'session' => $conn->id.'_'.$nome,
                            'quant_conexao' => $con+1
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }
            }
        }catch(\Exception $e){
            Log::error('Erro na API [method: InstanciaController@create]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function del(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'conexao.del';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = json_decode("{}");
            if($req['con']->token != ""){
                $dados = $whats->closeSession($req['con']->session, $req['con']->token);
                $dados = json_decode($dados);
            }else{
                $dados->status = true;
            }
            $req['con']->status = -1;
            if($req['con']->save() && $dados->status == true){
                $response = [
                    'error' => false,
                    'data' => [
                        'mensagem' => 'Conexão deletada'
                    ]
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response);
            }else if($req['con']->save() && $dados->status == false){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Essa instância foi deletada anteriormente.'
                    ]
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 400);
            }
        }catch(\Exception $e){
            Log::error('Erro na API [method: InstanciaController@del]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function qrcode(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'conexao.qrcode';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            if($req->input('type') === 'base64'){
                $dados = $whats->statusSession($req['con']->session, $req['con']->token);
                $dados = json_decode($dados);
                if(isset($dados->error) && $dados->error == true){
                    $response = [
                        "error" => false,
                        "data" => [
                            'mensagem' => $dados->status,
                            "qrcode" => ""
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }else if(isset($dados->qrcode) && $dados->qrcode == ""){
                    $response = [
                        "error" => false,
                        "data" => [
                            'mensagem' => "Não existe qrcode para scanear",
                            "qrcode" => ""
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }else{
                    $response = [
                        "error" => false,
                        "data" => [
                            'mensagem' => $dados->status,
                            "qrcode" => $dados->qrcode
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }
            }else if($req->input('type') === 'image'){
                $dados = $whats->statusSession($req['con']->session, $req['con']->token);
                $dados = json_decode($dados);
                if(isset($dados->error) && $dados->error == true){
                    $response = [
                        "error" => false,
                        "data" => [
                            'mensagem' => $dados->status,
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }else if(isset($dados->qrcode) && $dados->qrcode == ""){
                    $response = [
                        "error" => false,
                        "data" => [
                            'mensagem' => "Não existe qrcode para scanear",
                        ]
                    ];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return response()->json($response);
                }else{
                    $response = ['<img src="'.$dados->qrcode.'">'];
                    $hist = new Historico();
                    $hist->usuario_id = $u->id;
                    $hist->route = $route;
                    $hist->path = $path;
                    $hist->method = $method;
                    $hist->header = json_encode($header);
                    $hist->cookie = json_encode($cookie);
                    $hist->request = json_encode($request);
                    $hist->response = json_encode($response);
                    $hist->ip = $ip;
                    $hist->save();
                    return '<img src="'.$dados->qrcode.'">';
                }

            }
        }catch(\Exception $e){
            Log::error('Erro na API [method: InstanciaController@qrcode]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function checkConnection(Request $req) {
        try{
            $whats = new WhatsAppApi();
            $u = auth()->user();

            $route = 'api.checkConnection';
            $path = $req->path();
            $method = $req->method();
            $header = $req->header();
            $cookie = $req->cookie();
            $request = $req->all();
            $ip = $header["cf-connecting-ip"][0];

            $dados = $whats->checkConnectionSession($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);

            $response = [
                "error" => false,
                "data" => [
                    'mensagem' => $dados->message,
                ]
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: InstanciaController@checkConnection]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }
}
