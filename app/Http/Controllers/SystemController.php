<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Conexoes;
use App\Models\Empresas;

class SystemController extends Controller
{
    //
    public function index(Request $req) {
        $data = [];
        $data['link'] = false;
        return view('system.dashboard', $data);
    }

    public function campanha(Request $req) {
        $data = [];
        return view('system.campanha', $data);
    }

    public function conexoes(Request $req) {
        $data = [];
        $u = auth()->user();
        $e = Empresas::find($u->empresa_id);
        if($e !== null){
            $data['conexoes'] = Conexoes::where('empresa_id', $e->id)->where('status', '>=', 0)->get();
        }else{
            $data['conexoes'] = Conexoes::where('status', '>=', 0)->get();
        }
        return view('system.conexoes', $data);
    }

    public function conexoesStatus(Request $req) {
        $data = [];
        $data['c'] = Conexoes::find($req->id);
        return view('system.conexoesStatus', $data);
    }

    public function sair(Request $req) {
        Auth::logout();
        return view('logout');
    }

    public function changelog(Request $req) {
        return view('system.changelog');
    }

    public function createConexao(Request $req, $id = 0) {
        $u = auth()->user();
        $e = Empresas::find($u->empresa_id);
        try{
            $data = [];
            if($id != 0) $conexao = Conexoes::find($id);
            if($id == 0) $conexao = new Conexoes();
            $data['c'] = $conexao;

            if($req->isMethod('POST')){

                $conexao->fill($req->all());
                $req->input('chatwootUrl') == null ? $chatwootUrl = '' : $chatwootUrl = $req->input('chatwootUrl');
                $conexao->chatwootUrl = $chatwootUrl;
                $conexao->empresa_id = $e->id;
                $conexao->save();

                $req->session()->flash('success', 'Conexão criada com sucesso');
                return redirect()->route('conexoes');
            }
        }catch(\Exception $e){
            \Log::error('Erro ao criar a conexão [method: SystemController@createConexao]: ', [$e->getMessage()]);
            $req->session()->flash('fail', 'Erro ao criar a conexão');
            return redirect()->route('conexoes');
        }

        return view('system.createcon', $data);
    }
}
