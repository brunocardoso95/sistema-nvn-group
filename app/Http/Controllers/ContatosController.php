<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiClient\WhatsAppApi;
use App\Models\Historico;

class ContatosController extends Controller
{
    //
    public function checkNumberStatus(Request $req, $telefone = false) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'contact.checkNumberStatus';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->checkNumberStatus($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = $dados->response->status;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ContatosController@checkNumberStatus]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function allContacts(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'contact.allContacts';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allContacts($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ContatosController@allContacts]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function getContact(Request $req, $telefone = false) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'contact.getContact';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->getContact($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ContatosController@getContact]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function picturePic(Request $req, $telefone) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'contact.picturePic';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->getProfilePicture($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ContatosController@picturePic]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function profileStatus(Request $req, $telefone) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'contact.profileStatus';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->getProfileStatus($req['con']->session, $req['con']->token, $telefone);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: ContatosController@profileStatus]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }
}
