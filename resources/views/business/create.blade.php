<form method="POST" action="{{route('business.create')}}@if($e->id!=null)/{{$e->id}}@endif">
    @csrf
    <input type="hidden" name="id" id="id" value="{{$e->id}}">
    <div class="row">
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="cnpj" id='cnpj' placeholder="CNPJ" value="{{$e->maskCnpj()}}" required>
                <label for="cnpj">CNPJ</label>
            </div>
        </div>
        <div class="col-lg-8 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="razaosocial" id="razaosocial" placeholder="Razão Social" value="{{$e->razaosocial}}" required>
                <label for="razaosocial">Razão Social</label>
            </div>
        </div>
        <div class="col-lg-8 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="fantasia" id="fantasia" placeholder="Nome Fantasia" value="{{$e->fantasia}}">
                <label for="fantasia">Nome Fantasia</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="date" class="form-control form-control-lg" name="vencimento" id='vencimento' placeholder="Vencimento" value="{{$e->vencimento}}">
                <label for="vencimento">Vencimento</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="cpf_razaosocial" id='cpf_razaosocial' placeholder="CPF" value="{{$e->maskCpf()}}" required>
                <label for="cpf_razaosocial">CPF</label>
            </div>
        </div>
        <div class="col-lg-8 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="responsavel" id="responsavel" placeholder="Responsável" value="{{$e->responsavel}}" required>
                <label for="responsavel">Responsável</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="celular" id="celular" placeholder="Celular" value="{{$e->maskCel()}}" required>
                <label for="celular">Celular para Contato</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="telefone" id="telefone" placeholder="Telefone" value="{{$e->maskTel()}}">
                <label for="telefone">Telefone para Contato</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="number" class="form-control form-control-lg" name="conexao" id="conexao" placeholder="Conexao" value="{{$e->conexao}}" required>
                <label for="conexao">Quantidade de Conexão</label>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-3 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="cep" id="cep" placeholder="CEP" value="{{$e->cep}}" required>
                <label for="cep">CEP</label>
            </div>
        </div>
        <div class="col-lg-7 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="logradouro" id="logradouro" placeholder="logradouro" value="{{$e->logradouro}}" required>
                <label for="logradouro">Logradouro</label>
            </div>
        </div>
        <div class="col-lg-2 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="numero" id="numero" placeholder="numero" value="{{$e->numero}}" required>
                <label for="numero">Número</label>
            </div>
        </div>
        <div class="col-lg-3 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="complemento" id="complemento" placeholder="complemento" value="{{$e->complemento}}" required>
                <label for="complemento">Complemento</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="bairro" id="bairro" placeholder="bairro" value="{{$e->bairro}}" required>
                <label for="bairro">Bairro</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="cidade" id="cidade" placeholder="cidade" value="{{$e->cidade}}" required>
                <label for="cidade">Cidade</label>
            </div>
        </div>
        <div class="col-lg-1 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="uf" id="uf" placeholder="uf" value="{{$e->uf}}" required>
                <label for="uf">UF</label>
            </div>
        </div>
    </div>
    <hr>
    <div class="form-floating">
        <textarea class="form-control form-control-lg" name="observacao" id="observacao" cols="30" rows="10"></textarea>
        <label for="observacao">Observação da Empresa</label>
    </div>
</form>

<script>
    document.getElementById('cnpj').addEventListener('keyup', () => {
        const cnpj = (this).value;
        const result = validarCNPJ(cnpj);
        console.info(result)
        if(result === 0) {console.info('CNPJ Válido')} else {console.info('CNPJ Inválido')}
    })

    document.getElementById('cnpj').addEventListener('blur', () => {
        const cnpj = (this).value;
        const result = validarCNPJ(cnpj);
        console.info(result)
        if(result === 0) {console.info('CNPJ Válido')} else {console.info('CNPJ Inválido')}
    })
</script>
