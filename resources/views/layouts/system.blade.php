@php
$usuario = auth()->user();
@endphp
<!DOCTYPE html>
<html lang="pt-BR" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistemas NvN Group</title>

        <!-- ===============================================-->
        <!--    Favicons-->
        <!-- ===============================================-->
        <meta name="theme-color" content="#ffffff">
        <script src="{{asset('assets/js/config.js')}}"></script>
        <script src="{{asset('vendors/simplebar/simplebar.min.js')}}"></script>

        <!-- ===============================================-->
        <!--    Stylesheets-->
        <!-- ===============================================-->
        {{-- <link href="{{asset('vendors/leaflet/leaflet.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/leaflet.markercluster/MarkerCluster.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/leaflet.markercluster/MarkerCluster.Default.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/fullcalendar/main.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/flatpickr/flatpickr.min.css')}}" rel="stylesheet">

        <link href="{{asset('vendors/simplebar/simplebar.min.css')}}" rel="stylesheet"> --}}
        <link rel="preconnect" href="https://fonts.gstatic.com/">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
        <link href="https://site-assets.fontawesome.com/releases/v6.4.0/css/all.css" rel="stylesheet">
        <link href="{{asset('assets/css/theme.min.css')}}" rel="stylesheet" id="style-default">
        <link href="{{asset('assets/css/theme-rtl.min.css')}}" rel="stylesheet" id="style-rtl">
        <link href="{{asset('assets/css/user-rtl.min.css')}}" rel="stylesheet" id="user-style-rtl">
        <link href="{{asset('assets/css/user.min.css')}}" rel="stylesheet" id="user-style-default">
        <script>
            var isRTL = JSON.parse(localStorage.getItem('isRTL'));
            if (isRTL) {
                var linkDefault = document.getElementById('style-default');
                var userLinkDefault = document.getElementById('user-style-default');
                linkDefault.setAttribute('disabled', true);
                userLinkDefault.setAttribute('disabled', true);
                document.querySelector('html').setAttribute('dir', 'rtl');
            } else {
                var linkRTL = document.getElementById('style-rtl');
                var userLinkRTL = document.getElementById('user-style-rtl');
                linkRTL.setAttribute('disabled', true);
                userLinkRTL.setAttribute('disabled', true);
            }
        </script>
        <script src="{{asset('js/app.js')}}"></script>

        @yield('css')
    </head>

    <body>
        <!-- ===============================================-->
        <!--    Main Content-->
        <!-- ===============================================-->
        <main class="main" id="top">
            <div class="container" data-layout="container-fluid">
                <script>
                    var isFluid = JSON.parse(localStorage.getItem('isFluid'));
                    if (isFluid) {
                        var container = document.querySelector('[data-layout]');
                        container.classList.remove('container');
                        container.classList.add('container-fluid');
                    }
                </script>
                <nav class="navbar navbar-light navbar-vertical navbar-expand-xl" style="display: none;">
                    <script>
                        var navbarStyle = localStorage.getItem("navbarStyle");
                        if (navbarStyle && navbarStyle !== 'transparent') {
                            document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
                        }

                    </script>
                    <div class="d-flex align-items-center">
                        <div class="toggle-icon-wrapper">
                            <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle"
                                data-bs-toggle="tooltip" data-bs-placement="left" title="Toggle Navigation"><span
                                    class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
                        </div>
                        <a class="navbar-brand" href="{{route('dashboard')}}">
                            <div class="d-flex align-items-center py-3"><span class="font-sans-serif">NvN Group</span></div>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
                        <div class="navbar-vertical-content scrollbar">
                            <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
                                <li class="nav-item">

                                    <!-- label-->
                                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                                        <div class="col-auto navbar-vertical-label">Sistemas</div>
                                        <div class="col ps-0">
                                            <hr class="mb-0 navbar-vertical-divider" />
                                        </div>
                                    </div>
                                    <!-- Dashboard -->
                                    <a class="nav-link @if(Route::currentRouteName() == 'dashboard') active @endif" href="{{route('dashboard')}}" role="button" data-bs-toggle="" aria-expanded="false">
                                        <div class="d-flex align-items-center">
                                            <span class="nav-link-icon"><span class="fa-solid fa-chart-pie"></span></span>
                                            <span class="nav-link-text ps-1">Dashboard</span>
                                        </div>
                                    </a>
                                    <!-- label-->
                                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                                        <div class="col-auto navbar-vertical-label">WhatsApp</div>
                                        <div class="col ps-0">
                                            <hr class="mb-0 navbar-vertical-divider" />
                                        </div>
                                    </div>

                                    {{-- <!-- Campanha -->
                                    <a class="nav-link @if(Route::currentRouteName() == 'campanha') active @endif" href="{{route('campanha')}}" role="button" data-bs-toggle="" aria-expanded="false">
                                        <div class="d-flex align-items-center">
                                            <span class="nav-link-icon"><span class="fa-solid fa-megaphone"></span></span>
                                            <span class="nav-link-text ps-1">Campanha</span>
                                        </div>
                                    </a> --}}

                                    <!-- Conexões -->
                                    <a class="nav-link @if(Route::currentRouteName() == 'conexoes') active @endif" href="{{route('conexoes')}}" role="button" data-bs-toggle="" aria-expanded="false">
                                        <div class="d-flex align-items-center">
                                            <span class="nav-link-icon"><span class="fa-solid fa-mobile-screen"></span></span>
                                            <span class="nav-link-text ps-1">Conexões</span>
                                        </div>
                                    </a>

                                    @if($usuario->isAdmin())
                                    <!-- label-->
                                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                                        <div class="col-auto navbar-vertical-label">Administrativo</div>
                                        <div class="col ps-0">
                                            <hr class="mb-0 navbar-vertical-divider" />
                                        </div>
                                    </div>

                                    <!-- Empresas -->
                                    <a class="nav-link @if(Route::currentRouteName() == 'business') active @endif" href="{{route('business')}}" role="button" data-bs-toggle="" aria-expanded="false">
                                        <div class="d-flex align-items-center">
                                            <span class="nav-link-icon"><span class="fa-solid fa-briefcase"></span></span>
                                            <span class="nav-link-text ps-1">Empresas</span>
                                        </div>
                                    </a>

                                    <!-- Usuários -->
                                    <a class="nav-link @if(Route::currentRouteName() == 'users') active @endif" href="{{route('users')}}" role="button" data-bs-toggle="" aria-expanded="false">
                                        <div class="d-flex align-items-center">
                                            <span class="nav-link-icon"><span class="fa-solid fa-users"></span></span>
                                            <span class="nav-link-text ps-1">Usuários</span>
                                        </div>
                                    </a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand-xl" style="display: none;">
                    <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button"
                        data-bs-toggle="collapse" data-bs-target="#navbarStandard" aria-controls="navbarStandard"
                        aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span
                                class="toggle-line"></span></span></button>
                    <a class="navbar-brand me-1 me-sm-3" href="{{route('dashboard')}}">
                        <div class="d-flex align-items-center"><span class="font-sans-serif">NvN Group</span></div>
                    </a>

                    <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
                        <li class="nav-item">
                            <div class="theme-control-toggle fa-icon-wait px-2">
                                <input class="form-check-input ms-0 theme-control-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark" />
                                <label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Trocar por Tema Claro">
                                    <span class="fa-solid fa-sun fs-0"></span>
                                </label>
                                <label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Trocar por Tema Escuro">
                                    <span class="fa-solid fa-moon fs-0"></span>
                                </label>
                            </div>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a class="nav-link px-0 notification-indicator notification-indicator-warning notification-indicator-fill fa-icon-wait"
                                href="../app/e-commerce/shopping-cart.html"><span class="fas fa-shopping-cart"
                                    data-fa-transform="shrink-7" style="font-size: 33px;"></span><span
                                    class="notification-indicator-number">1</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link notification-indicator notification-indicator-primary px-0 fa-icon-wait"
                                id="navbarDropdownNotification" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" data-hide-on-body-scroll="data-hide-on-body-scroll"><span
                                    class="fa-solid fa-bell" data-fa-transform="shrink-6" style="font-size: 33px;"></span></a>
                            <div class="dropdown-menu dropdown-caret dropdown-caret dropdown-menu-end dropdown-menu-card dropdown-menu-notification dropdown-caret-bg"
                                aria-labelledby="navbarDropdownNotification">
                                <div class="card card-notification shadow-none">
                                    <div class="card-header">
                                        <div class="row justify-content-between align-items-center">
                                            <div class="col-auto">
                                                <h6 class="card-header-title mb-0">Notifications</h6>
                                            </div>
                                            <div class="col-auto ps-0 ps-sm-3"><a class="card-link fw-normal" href="#">Mark
                                                    all as read</a></div>
                                        </div>
                                    </div>
                                    <div class="scrollbar-overlay" style="max-height:19rem">
                                        <div class="list-group list-group-flush fw-normal fs--1">
                                            <div class="list-group-title border-bottom">NEW</div>
                                            <div class="list-group-item">
                                                <a class="notification notification-flush notification-unread" href="#!">
                                                    <div class="notification-avatar">
                                                        <div class="avatar avatar-2xl me-3">
                                                            <img class="rounded-circle"
                                                                src="{{ asset('assets/img/team/1-thumb.png')}}" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="notification-body">
                                                        <p class="mb-1"><strong>Emma Watson</strong> replied to your comment
                                                            : "Hello world 😍"</p>
                                                        <span class="notification-time"><span class="me-2" role="img"
                                                                aria-label="Emoji">💬</span>Just now</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="list-group-item">
                                                <a class="notification notification-flush notification-unread" href="#!">
                                                    <div class="notification-avatar">
                                                        <div class="avatar avatar-2xl me-3">
                                                            <div class="avatar-name rounded-circle"><span>AB</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="notification-body">
                                                        <p class="mb-1"><strong>Albert Brooks</strong> reacted to
                                                            <strong>Mia Khalifa's</strong> status</p>
                                                        <span class="notification-time"><span
                                                                class="me-2 fab fa-gratipay text-danger"></span>9hr</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="list-group-title border-bottom">EARLIER</div>
                                            <div class="list-group-item">
                                                <a class="notification notification-flush" href="#!">
                                                    <div class="notification-avatar">
                                                        <div class="avatar avatar-2xl me-3">
                                                            <img class="rounded-circle"
                                                                src="{{ asset('assets/img/icons/weather-sm.jpg')}}"
                                                                alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="notification-body">
                                                        <p class="mb-1">The forecast today shows a low of 20&#8451; in
                                                            California. See today's weather.</p>
                                                        <span class="notification-time"><span class="me-2" role="img"
                                                                aria-label="Emoji">🌤️</span>1d</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="list-group-item">
                                                <a class="border-bottom-0 notification-unread  notification notification-flush"
                                                    href="#!">
                                                    <div class="notification-avatar">
                                                        <div class="avatar avatar-xl me-3">
                                                            <img class="rounded-circle"
                                                                src="{{ asset('assets/img/logos/oxford.png')}}" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="notification-body">
                                                        <p class="mb-1"><strong>University of Oxford</strong> created an
                                                            event : "Causal Inference Hilary 2019"</p>
                                                        <span class="notification-time"><span class="me-2" role="img"
                                                                aria-label="Emoji">✌️</span>1w</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="list-group-item">
                                                <a class="border-bottom-0 notification notification-flush" href="#!">
                                                    <div class="notification-avatar">
                                                        <div class="avatar avatar-xl me-3">
                                                            <img class="rounded-circle"
                                                                src="{{ asset('assets/img/team/10.jpg')}}" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="notification-body">
                                                        <p class="mb-1"><strong>James Cameron</strong> invited to join the
                                                            group: United Nations International Children's Fund</p>
                                                        <span class="notification-time"><span class="me-2" role="img"
                                                                aria-label="Emoji">🙋‍</span>2d</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center border-top"><a class="card-link d-block"
                                            href="../app/social/notifications.html">View all</a></div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown px-1">
                            <a class="nav-link fa-icon-wait nine-dots p-1" id="navbarDropdownMenu" role="button"
                                data-hide-on-body-scroll="data-hide-on-body-scroll" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg"
                                    width="16" height="43" viewBox="0 0 16 16" fill="none">
                                    <circle cx="2" cy="2" r="2" fill="#6C6E71"></circle>
                                    <circle cx="2" cy="8" r="2" fill="#6C6E71"></circle>
                                    <circle cx="2" cy="14" r="2" fill="#6C6E71"></circle>
                                    <circle cx="8" cy="8" r="2" fill="#6C6E71"></circle>
                                    <circle cx="8" cy="14" r="2" fill="#6C6E71"></circle>
                                    <circle cx="14" cy="8" r="2" fill="#6C6E71"></circle>
                                    <circle cx="14" cy="14" r="2" fill="#6C6E71"></circle>
                                    <circle cx="8" cy="2" r="2" fill="#6C6E71"></circle>
                                    <circle cx="14" cy="2" r="2" fill="#6C6E71"></circle>
                                </svg></a>
                            <div class="dropdown-menu dropdown-caret dropdown-caret dropdown-menu-end dropdown-menu-card dropdown-caret-bg"
                                aria-labelledby="navbarDropdownMenu">
                                <div class="card shadow-none">
                                    <div class="scrollbar-overlay nine-dots-dropdown">
                                        <div class="card-body px-3">
                                            <div class="row text-center gx-0 gy-0">
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="../pages/user/profile.html" target="_blank">
                                                        <div class="avatar avatar-2xl"> <img class="rounded-circle"
                                                                src="{{ asset('assets/img/team/3.jpg')}}" alt="" /></div>
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2">Account</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="https://themewagon.com/" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/themewagon.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">
                                                            Themewagon</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="https://mailbluster.com/" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/mailbluster.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">
                                                            Mailbluster</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/google.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Google
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/spotify.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Spotify
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/steam.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Steam
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/github-light.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Github
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/discord.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Discord
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/xbox.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">xbox</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/trello.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Kanban
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/hp.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Hp</p>
                                                    </a></div>
                                                <div class="col-12">
                                                    <hr class="my-3 mx-n3 bg-200" />
                                                </div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/linkedin.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Linkedin
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/twitter.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Twitter
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/facebook.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Facebook
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/instagram.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">
                                                            Instagram</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/pinterest.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">
                                                            Pinterest</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/slack.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">Slack
                                                        </p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="#!" target="_blank"><img class="rounded"
                                                            src="{{ asset('assets/img/nav-icons/deviantart.png')}}" alt=""
                                                            width="40" height="40" />
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2 pt-1">
                                                            Deviantart</p>
                                                    </a></div>
                                                <div class="col-4"><a
                                                        class="d-block hover-bg-200 px-2 py-3 rounded-3 text-center text-decoration-none"
                                                        href="../app/events/event-detail.html" target="_blank">
                                                        <div class="avatar avatar-2xl">
                                                            <div
                                                                class="avatar-name rounded-circle bg-soft-primary text-primary">
                                                                <span class="fs-2">E</span></div>
                                                        </div>
                                                        <p class="mb-0 fw-medium text-800 text-truncate fs--2">Events</p>
                                                    </a></div>
                                                <div class="col-12"><a class="btn btn-outline-primary btn-sm mt-4"
                                                        href="#!">Show more</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link pe-0 ps-2" id="navbarDropdownUser" role="button"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="avatar avatar-xl">
                                    <img class="rounded-circle" src="{{ asset('assets/img/team/3-thumb.png')}}" alt="" />
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-caret dropdown-caret dropdown-menu-end py-0"
                                aria-labelledby="navbarDropdownUser">
                                <div class="bg-white dark__bg-1000 rounded-2 py-2">
                                    <a class="dropdown-item fw-bold text-warning" href="#!"><span
                                            class="fas fa-crown me-1"></span><span>Go Pro</span></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#!">Set status</a>
                                    <a class="dropdown-item" href="../pages/user/profile.html">Profile &amp; account</a>
                                    <a class="dropdown-item" href="#!">Feedback</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../pages/user/settings.html">Settings</a>
                                    <a class="dropdown-item" href="{{route('sair')}}">Sair</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
                <div class="content">
                    <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand" style="display: none;">
                        <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button"
                            data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse"
                            aria-controls="navbarVerticalCollapse" aria-expanded="false"
                            aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span
                                    class="toggle-line"></span></span></button>
                        <a class="navbar-brand me-1 me-sm-3" href="{{route('dashboard')}}">
                            <div class="d-flex align-items-center">
                                <span class="font-sans-serif">NvN Group</span></div>
                        </a>
                        <ul class="navbar-nav align-items-center d-none d-lg-block">
                            <li class="nav-item">
                                <div class="search-box" data-list='{"valueNames":["title"]}'>
                                    <form class="position-relative" data-bs-toggle="search" data-bs-display="static">
                                        <input class="form-control search-input fuzzy-search" type="search" placeholder="Procurar..." aria-label="Search" />
                                        <span class="fas fa-search search-box-icon"></span>
                                    </form>
                                    <div class="btn-close-falcon-container position-absolute end-0 top-50 translate-middle shadow-none" data-bs-dismiss="search">
                                        <button class="btn btn-link btn-close-falcon p-0" aria-label="Close"></button>
                                    </div>
                                    <div class="dropdown-menu border font-base start-0 mt-2 py-0 overflow-hidden w-100">
                                        <div class="scrollbar list py-3" style="max-height: 24rem;">
                                            <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Pesquisado recentemente</h6>
                                            <div id="search-last"></div>
                                            <hr class="text-200 dark__text-900" />
                                            <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Sua pesquisa</h6>
                                            <div id="search-box"></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
                            <li class="nav-item">
                                <div class="theme-control-toggle fa-icon-wait px-2">
                                    <input class="form-check-input ms-0 theme-control-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark" />
                                        <label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Trocar por Tema Claro">
                                        <span class="fa-solid fa-sun fs-0"></span>
                                    </label>
                                    <label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Trocar por Tema Escuro">
                                        <span class="fa-solid fa-moon fs-0"></span>
                                    </label>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link notification_flag notification-indicator-primary px-0 fa-icon-wait" id="navbarDropdownNotification" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-hide-on-body-scroll="data-hide-on-body-scroll">
                                    <span class="fa-solid fa-bell" data-fa-transform="shrink-6" style="font-size: 33px;"></span>
                                </a>
                                <div class="dropdown-menu dropdown-caret dropdown-caret dropdown-menu-end dropdown-menu-card dropdown-menu-notification dropdown-caret-bg" aria-labelledby="navbarDropdownNotification">
                                    <div class="card card-notification shadow-none">
                                        <div class="card-header">
                                            <div class="row justify-content-between align-items-center">
                                                <div class="col-auto">
                                                    <h6 class="card-header-title mb-0">Notificações</h6>
                                                </div>
                                                <div class="col-auto ps-0 ps-sm-3">
                                                    <a class="card-link fw-normal notification_read" href="#">Marcar todas como lida</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="scrollbar-overlay" style="max-height:19rem">
                                            <div class="list-group list-group-flush fw-normal fs--1">
                                                <div class="list-group-title border-bottom notification-title-new d-none">NOVO</div>
                                                <div class="list-group-item notification-list-new">

                                                </div>
                                                <div class="list-group-title border-bottom">ANTIGOS</div>
                                                <div class="list-group-item">
                                                    <a class="notification notification-flush" href="#!">
                                                        <div class="notification-avatar">
                                                            <div class="avatar avatar-2xl me-3">
                                                                <img class="rounded-circle"
                                                                    src="{{ asset('assets/img/icons/weather-sm.jpg')}}"
                                                                    alt="" />
                                                            </div>
                                                        </div>
                                                        <div class="notification-body">
                                                            <p class="mb-1">The forecast today shows a low of 20&#8451; in
                                                                California. See today's weather.</p>
                                                            <span class="notification-time"><span class="me-2" role="img"
                                                                    aria-label="Emoji">🌤️</span>1d</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-center border-top">
                                            <a class="card-link d-block" href="../app/social/notifications.html">Ver notificações</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="nav-item dropdown"><a class="nav-link pe-0 ps-2" id="navbarDropdownUser"
                                    role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="avatar avatar-xl">
                                        <img class="rounded-circle" src="{{ asset('assets/img/team/3-thumb.png')}}" alt="" />
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-caret dropdown-caret dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
                                    <div class="bg-white dark__bg-1000 rounded-2 py-2">
                                        <a class="dropdown-item fw-bold text-warning" href="#!">
                                            <span class="fas fa-crown me-1"></span><span>Upgrade</span>
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#!">Perfil</a>
                                        <a class="dropdown-item" href="#!">Alterar status</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{route('users.configuracoes')}}">Configurações</a>
                                        <a class="dropdown-item" href="{{route('sair')}}">Sair</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <script>
                        var navbarPosition = localStorage.getItem('navbarPosition');
                        var navbarVertical = document.querySelector('.navbar-vertical');
                        var navbarTopVertical = document.querySelector('.content .navbar-top');
                        var navbarTop = document.querySelector('[data-layout] .navbar-top');
                        var navbarTopCombo = document.querySelector('.content [data-navbar-top="combo"]');
                        if (navbarPosition === 'top') {
                            navbarTop.removeAttribute('style');
                            navbarTopVertical.remove(navbarTopVertical);
                            navbarVertical.remove(navbarVertical);
                            navbarTopCombo.remove(navbarTopCombo);
                        } else if (navbarPosition === 'combo') {
                            navbarVertical.removeAttribute('style');
                            navbarTopCombo.removeAttribute('style');
                            navbarTop.remove(navbarTop);
                            navbarTopVertical.remove(navbarTopVertical);
                        } else {
                            navbarVertical.removeAttribute('style');
                            navbarTopVertical.removeAttribute('style');
                            navbarTop.remove(navbarTop);
                            //navbarTopCombo.remove(navbarTopCombo);
                        }
                    </script>

                    @yield('content')

                    <footer class="footer">
                        <div class="row g-0 justify-content-between fs--1 mt-4 mb-3">
                            <div class="col-12 col-sm-auto text-center">
                                <p class="mb-0 text-600">Obrigado por usar o Sistemas NvN Group
                                    <span class="d-none d-sm-inline-block">| </span>
                                    <br class="d-sm-none" /> 2022-{{date('Y')}} &copy;
                                    <a href="https://nvngroup.com.br/">NvN Group</a>
                                </p>
                            </div>
                            <div class="col-12 col-sm-auto text-center">
                                <p class="mb-0 text-600"><a href="{{route('changelog')}}" target="_blank">{{env('VERSION')}}</a></p>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </main>
        <!-- ===============================================-->
        <!--    End of Main Content-->
        <!-- ===============================================-->

        <!-- ===============================================-->
        <!--    JavaScripts-->
        <!-- ===============================================-->


        <div class="toast-container position-fixed top-0 end-0 p-3" style="z-index: 9999999">
            <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <strong class="me-auto" id="toastTitle"></strong>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body" id='toastText'></div>
            </div>
        </div>

        <div class="modal fade" id="modal" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" id="tamanho">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" id="limpar" class="btn btn-danger">Limpar</button>
                        <button type="button" class="btn btn-primary" id="saveModal">Salvar alteração</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('vendors/popper/popper.min.js')}}"></script>
        <script src="{{asset('vendors/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{asset('vendors/anchorjs/anchor.min.js')}}"></script>
        <script src="{{asset('vendors/is/is.min.js')}}"></script>
        <script src="{{asset('vendors/chart/chart.min.js')}}"></script>
        <script src="{{asset('vendors/leaflet/leaflet.js')}}"></script>
        <script src="{{asset('vendors/leaflet.markercluster/leaflet.markercluster.js')}}"></script>
        <script src="{{asset('vendors/leaflet.tilelayer.colorfilter/leaflet-tilelayer-colorfilter.min.js')}}"></script>
        <script src="{{asset('vendors/countup/countUp.umd.js')}}"></script>
        <script src="{{asset('vendors/echarts/echarts.min.js')}}"></script>
        <script src="{{asset('vendors/fullcalendar/main.min.js')}}"></script>
        <script src="{{asset('assets/js/flatpickr.js')}}"></script>
        <script src="{{asset('vendors/dayjs/dayjs.min.js')}}"></script>
        <script src="{{asset('vendors/fontawesome/all.min.js')}}"></script>
        <script src="{{asset('vendors/lodash/lodash.min.js')}}"></script>
        <script src="{{asset('vendors/polyfill.io/v3/polyfill.min58be.js?features=window.scroll')}}"></script>
        <script src="{{asset('vendors/list.js/list.min.js')}}"></script>
        <script src="{{asset('vendors/dropzone/dropzone.min.js')}}"></script>
        <script src="{{asset('assets/js/theme.js')}}"></script>
        <script src="{{asset('assets/js/cunstons.js')}}"></script>

        <script type="text/javascript">
            const myModal = new bootstrap.Modal(document.getElementById('modal'), null)
            function openComponentModal(url, titulo, tamanho, formModal){
                var modalHead = document.getElementById('modal-title')
                var modalBody = document.getElementById('modal-body')
                var modalTamanho = document.getElementById('tamanho')
                modalHead.innerHTML = titulo

                if(tamanho){
                    modalTamanho.classList.add('modal-' + tamanho);
                }

                if(url){
                    var xhttp = new XMLHttpRequest();
                    xhttp.open("GET", url, false);
                    xhttp.send();

                    modalBody.innerHTML = xhttp.responseText
                    form = document.getElementById(formModal)
                }
                myModal.show()
            }

            document.getElementById('saveModal').addEventListener('click', (e) => {
                const modalBody = document.getElementById('modal-body')
                const form = modalBody.querySelector('form');
                for(const el of form.querySelectorAll('[required]')){
                    if(!el.reportValidity()){
                        return false;
                    }
                }
                form.submit()
            })

            document.getElementById("limpar").onclick = function (event) {
                const modalBody = document.getElementById('modal-body')
                const forms = modalBody.querySelector('form');
                forms.reset();
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.classList.remove('was-validated');
                });
            };
        </script>
        @yield('js')
    </body>
</html>
