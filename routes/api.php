<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\InstanciaController;
use App\Http\Controllers\ConversaController;
use App\Http\Controllers\ContatosController;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\WebhookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('/status',                                               [ApiController::class, 'status'])->name('api.status');
Route::post('/webhook/{session}',                                   [WebhookController::class, 'webhook'])->name('api.webhook');
Route::post('/chatwoot/{session}',                                  [WebhookController::class, 'chatwoot'])->name('api.chatwoot');

Route::middleware(['auth.basic', 'userApi'])->prefix('v1')->group(function () {
    /*Instância*/
    Route::post('/init',                                            [InstanciaController::class, 'init'])->name('conexao.init');
    Route::put('/create',                                           [InstanciaController::class, 'create'])->name('conexao.create');

    Route::middleware(['conexaoAtiva'])->group(function() {
        /* Conexões */
        Route::get('/qrcode',                                       [InstanciaController::class, 'qrcode'])->name('conexao.qrcode');
        Route::get('/checkconnection',                              [InstanciaController::class, 'checkConnection'])->name('conexao.checkConnection');
        Route::delete('/del',                                       [InstanciaController::class, 'del'])->name('conexao.del');
        Route::delete('/delelete',                                  [InstanciaController::class, 'del'])->name('conexao.del2');

        /*Envio de Mensagem*/
        Route::match(['post', 'put'],'/enviar-mensagem',            [ConversaController::class, 'enviarMensagem'])->name('chat.enviarMensagem');
        Route::match(['post', 'put'],'/enviar-resposta',            [ConversaController::class, 'enviarReply'])->name('chat.enviarReply');
        Route::match(['post', 'put'],'/enviar-arquivo-base64',      [ConversaController::class, 'enviarFileBase64'])->name('chat.enviarFileBase64');
        Route::match(['post', 'put'],'/enviar-imagem-base64',       [ConversaController::class, 'enviarImageBase64'])->name('chat.enviarImageBase64');
        Route::match(['post', 'put'],'/enviar-audio-base64',        [ConversaController::class, 'enviarAudioBase64'])->name('chat.enviarAudioBase64');
        Route::match(['post', 'put'],'/enviar-botoes',              [ConversaController::class, 'enviarButtons'])->name('chat.enviarButtons');
        Route::match(['post', 'put'],'/enviar-contato',             [ConversaController::class, 'enviarContato'])->name('chat.enviarContato');
        Route::match(['post', 'put'],'/enviar-link',                [ConversaController::class, 'enviarLink'])->name('chat.enviarLink');
        Route::match(['post', 'put'],'/enviar-enquete',             [ConversaController::class, 'enviarEnquete'])->name('chat.enviarEnquete');
        Route::match(['post', 'put'],'/enviar-localizacao',         [ConversaController::class, 'enviarLocalizacao'])->name('chat.enviarLocalizacao');
        Route::match(['post', 'put'],'/enviar-sticker',             [ConversaController::class, 'enviarSticker'])->name('chat.enviarSticker');

        /* Chat */
        Route::get('/lista-bloqueados',                             [ChatController::class, 'blockList'])->name('chat.blockList');
        Route::post('/bloquear-contato',                            [ChatController::class, 'blockContact'])->name('chat.blockContact');
        Route::post('/desbloquear-contato',                         [ChatController::class, 'unblockContact'])->name('chat.unblockContact');
        Route::get('/todos-chat',                                   [ChatController::class, 'allChat'])->name('chat.allChat');
        Route::get('/todos-chat-com-mensagem',                      [ChatController::class, 'allChatWithMessage'])->name('chat.allChatWithMessage');
        Route::post('/chat-por-telefone',                           [ChatController::class, 'chatById'])->name('chat.chatById');
        Route::post('/chat-online',                                 [ChatController::class, 'chatIsOnline'])->name('chat.chatIsOnline');
        Route::post('/buscar-mensagem',                             [ChatController::class, 'getMessages'])->name('chat.getMessages');
        Route::post('/buscar-media-por-mensagem',                   [ChatController::class, 'getMediaByMessage'])->name('chat.getMediaByMessage');
        Route::get('/todas-novas-mensagens',                        [ChatController::class, 'allNewMessages'])->name('chat.allNewMessages');
        Route::get('/mensagens-nao-lidas',                          [ChatController::class, 'unreadMessages'])->name('chat.unreadMessages');
        Route::get('/todas-mensagens-nao-lidas',                    [ChatController::class, 'allUnreadMessages'])->name('chat.allUnreadMessages');
        Route::get('/ultima-visualizacao/{telefone?}',              [ChatController::class, 'lastSeen'])->name('chat.lastSeen');

        /* Contatos */
        Route::get('/todos-contatos',                               [ContatosController::class, 'allContacts'])->name('contact.allContacts');
        Route::get('/checar-numero/{telefone?}',                    [ContatosController::class, 'checkNumberStatus'])->name('contact.checkNumberStatus');
        Route::get('/contato/{telefone?}',                          [ContatosController::class, 'getContact'])->name('contact.getContact');
        Route::get('/contato-foto/{telefone?}',                     [ContatosController::class, 'picturePic'])->name('contact.picturePic');
        Route::get('/contato-status/{telefone?}',                   [ContatosController::class, 'profileStatus'])->name('contact.profileStatus');

        /* Grupo */
        Route::get('/todos-grupos',                                 [GrupoController::class, 'getAllGroups'])->name('group.getAllGroups');
        Route::get('/admin-do-grupo/{idgrupo?}',                    [GrupoController::class, 'groupAdmins'])->name('group.groupAdmins');
        Route::get('/todas-listas-transmissao',                     [GrupoController::class, 'getAllBroadcastList'])->name('group.getAllBroadcastList');
        Route::post('/criar-grupo',                                 [GrupoController::class, 'createGroup'])->name('group.createGroup');
        Route::post('/entrar-grupo-com-codigo',                     [GrupoController::class, 'joinGroupByCode'])->name('group.joinGroupByCode');
        Route::post('/adicionar-participante-grupo',                [GrupoController::class, 'addParticipantGroup'])->name('group.addParticipantGroup');
        Route::post('/remover-participante-grupo',                  [GrupoController::class, 'removeParticipantGroup'])->name('group.removeParticipantGroup');
        Route::post('/promover-participante-grupo',                 [GrupoController::class, 'promoteParticipantGroup'])->name('group.promoteParticipantGroup');
        Route::post('/revogar-participante-grupo',                  [GrupoController::class, 'demoteParticipantGroup'])->name('group.demoteParticipantGroup');
        Route::post('/informacao-do-grupo-com-codigo',              [GrupoController::class, 'groupInfoFromInviteLink'])->name('group.groupInfoFromInviteLink');

        /* Perfil */
        Route::post('/trocar-nome',                                 [ChatController::class, 'changeUsername'])->name('chat.changeUsername');
        Route::post('/trocar-status',                               [ChatController::class, 'profileStatus'])->name('chat.profileStatus');
    });
});

Route::middleware(['auth.basic', 'userApi'])->prefix('v1.1')->group(function () {
    /*Instância*/
    Route::post('/init',                                            [InstanciaController::class, 'init'])->name('conexao.init');
    Route::put('/create',                                           [InstanciaController::class, 'create'])->name('conexao.create');

    Route::middleware(['conexaoAtiva'])->group(function() {
        /* Conexões */
        Route::get('/qrcode',                                       [InstanciaController::class, 'qrcode'])->name('conexao.qrcode');
        Route::get('/checkconnection',                              [InstanciaController::class, 'checkConnection'])->name('conexao.checkConnection');
        Route::delete('/del',                                       [InstanciaController::class, 'del'])->name('conexao.del');
        Route::delete('/delelete',                                  [InstanciaController::class, 'del'])->name('conexao.del2');

        /*Envio de Mensagem*/
        Route::match(['post', 'put'],'/enviar-mensagem',            [ConversaController::class, 'enviarMensagem'])->name('chat.enviarMensagem');
        Route::match(['post', 'put'],'/enviar-resposta',            [ConversaController::class, 'enviarReply'])->name('chat.enviarReply');
        Route::match(['post', 'put'],'/enviar-arquivo-base64',      [ConversaController::class, 'enviarFileBase64'])->name('chat.enviarFileBase64');
        Route::match(['post', 'put'],'/enviar-imagem-base64',       [ConversaController::class, 'enviarImageBase64'])->name('chat.enviarImageBase64');
        Route::match(['post', 'put'],'/enviar-audio-base64',        [ConversaController::class, 'enviarAudioBase64'])->name('chat.enviarAudioBase64');
        Route::match(['post', 'put'],'/enviar-contato',             [ConversaController::class, 'enviarContato'])->name('chat.enviarContato');
        Route::match(['post', 'put'],'/enviar-link',                [ConversaController::class, 'enviarLink'])->name('chat.enviarLink');
        Route::match(['post', 'put'],'/enviar-enquete',             [ConversaController::class, 'enviarEnquete'])->name('chat.enviarEnquete');
        Route::match(['post', 'put'],'/enviar-localizacao',         [ConversaController::class, 'enviarLocalizacao'])->name('chat.enviarLocalizacao');
        Route::match(['post', 'put'],'/enviar-sticker',             [ConversaController::class, 'enviarSticker'])->name('chat.enviarSticker');

        /* Chat */
        Route::get('/lista-bloqueados',                             [ChatController::class, 'blockList'])->name('chat.blockList');
        Route::post('/bloquear-contato',                            [ChatController::class, 'blockContact'])->name('chat.blockContact');
        Route::post('/desbloquear-contato',                         [ChatController::class, 'unblockContact'])->name('chat.unblockContact');
        Route::post('/listar-chats',                                [ChatController::class, 'listChats'])->name('chat.listChats');
        Route::get('/todos-chats-arquivados',                       [ChatController::class, 'allChatArchived'])->name('chat.allChatArchived');
        Route::post('/chat-por-telefone',                           [ChatController::class, 'chatById'])->name('chat.chatById');
        Route::post('/chat-online',                                 [ChatController::class, 'chatIsOnline'])->name('chat.chatIsOnline');
        Route::post('/buscar-mensagem',                             [ChatController::class, 'getMessages'])->name('chat.getMessages');
        Route::post('/buscar-media-por-mensagem',                   [ChatController::class, 'getMediaByMessage'])->name('chat.getMediaByMessage');
        Route::get('/todas-novas-mensagens',                        [ChatController::class, 'allNewMessages'])->name('chat.allNewMessages');
        Route::get('/mensagens-nao-lidas',                          [ChatController::class, 'unreadMessages'])->name('chat.unreadMessages');
        Route::get('/todas-mensagens-nao-lidas',                    [ChatController::class, 'allUnreadMessages'])->name('chat.allUnreadMessages');
        Route::get('/ultima-visualizacao/{telefone?}',              [ChatController::class, 'lastSeen'])->name('chat.lastSeen');

        /* Contatos */
        Route::get('/todos-contatos',                               [ContatosController::class, 'allContacts'])->name('contact.allContacts');
        Route::get('/checar-numero/{telefone?}',                    [ContatosController::class, 'checkNumberStatus'])->name('contact.checkNumberStatus');
        Route::get('/contato/{telefone?}',                          [ContatosController::class, 'getContact'])->name('contact.getContact');
        Route::get('/contato-foto/{telefone?}',                     [ContatosController::class, 'picturePic'])->name('contact.picturePic');
        Route::get('/contato-status/{telefone?}',                   [ContatosController::class, 'profileStatus'])->name('contact.profileStatus');

        /* Grupo */
        Route::get('/admin-do-grupo/{idgrupo?}',                    [GrupoController::class, 'groupAdmins'])->name('group.groupAdmins');
        Route::get('/todas-listas-transmissao',                     [GrupoController::class, 'getAllBroadcastList'])->name('group.getAllBroadcastList');
        Route::post('/criar-grupo',                                 [GrupoController::class, 'createGroup'])->name('group.createGroup');
        Route::post('/entrar-grupo-com-codigo',                     [GrupoController::class, 'joinGroupByCode'])->name('group.joinGroupByCode');
        Route::post('/adicionar-participante-grupo',                [GrupoController::class, 'addParticipantGroup'])->name('group.addParticipantGroup');
        Route::post('/remover-participante-grupo',                  [GrupoController::class, 'removeParticipantGroup'])->name('group.removeParticipantGroup');
        Route::post('/promover-participante-grupo',                 [GrupoController::class, 'promoteParticipantGroup'])->name('group.promoteParticipantGroup');
        Route::post('/revogar-participante-grupo',                  [GrupoController::class, 'demoteParticipantGroup'])->name('group.demoteParticipantGroup');
        Route::post('/informacao-do-grupo-com-codigo',              [GrupoController::class, 'groupInfoFromInviteLink'])->name('group.groupInfoFromInviteLink');

        /* Perfil */
        Route::post('/trocar-nome',                                 [ChatController::class, 'changeUsername'])->name('chat.changeUsername');
        Route::post('/trocar-status',                               [ChatController::class, 'profileStatus'])->name('chat.profileStatus');
    });
});

Route::middleware(['auth.basic', 'isAdminApi'])->group(function() {
    /*Empresas*/
    Route::get('/business/{id?}',                                   [EmpresaController::class, 'apiBusiness'])->name('api.apiBusiness');
    Route::put('/business/{id?}',                                   [EmpresaController::class, 'apiBusinessCreate'])->name('api.apiBusiness.create');
    Route::delete('/business/{id}',                                 [EmpresaController::class, 'apiBusinessDelete'])->name('api.apiBusiness.delete');

    /*Usuários*/
    Route::get('/users/{id?}',                                      [UsuarioController::class, 'apiUsers'])->name('api.apiUsers');
    Route::put('/users/{id?}',                                      [UsuarioController::class, 'apiUsersCreate'])->name('api.apiUsers.create');
});
