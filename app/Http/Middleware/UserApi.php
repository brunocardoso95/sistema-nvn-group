<?php

namespace App\Http\Middleware;

use App\Models\Empresas;
use Closure;
use Illuminate\Http\Request;
use \App\Models\Historico;

class UserApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $req, Closure $next)
    {
        $u = auth()->user();
        $route = 'ConexaoAtiva';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        if($u->api == 0){
            $response = [
                'error' => true,
                'data' => [
                    'mensagem' => 'Você não está autorizado a usar a API.',
                ],
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, 401);
        }
        $e = Empresas::find($u->empresa_id);
        if($e->status < 1){
            $response = [
                'error' => true,
                'data' => [
                    'mensagem' => 'Sua empresa se encontra suspensa, favor entrar em contato com o número: +55 21 2042-8610 para maiores detalhes.'
                ],
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, 402);
        }
        return $next($req);
    }
}
