@extends('layouts.system')

@section('content')
<div class="container-fluid">
    <nav style="--falcon-breadcrumb-divider: '»';" aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Administrativo</a></li>
        <li class="breadcrumb-item active" aria-current="page">Empresas</li>
        <li class="breadcrumb-item"><a href="#" id="addEmpresa" class="me-1"><i class="align-middle fa-solid fa-add"></i></a></li>
        </ol>
    </nav>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table id="datatables-clients" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Empresa</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($usuarios) && count($usuarios)>0)
                            @foreach($usuarios as $u)
                            <tr>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>{{ $u->empresa->fantasia }}</td>
                                <td>
                                    {{-- <button class="btn btn-sm btn-default editUsuario" data-empresa="{{ $u->id }}" data-url="{{route('business.create', ['id' => $u->id ])}}"><i class="fas fa-pen"></i></button> --}}
                                    <button class="btn btn-sm btn-danger deleteUsuario" data-empresa="{{ $u->id }}" data-url=""><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
{{-- <script>
    document.getElementById('addUsuario').addEventListener('click', () => {
        openComponentModal('{{route('users.create')}}', 'Criar Empresa', 'xl', 'usuarios.criarForm')
    })

    /*const editEmpresas = document.querySelectorAll('.editEmpresa');
    editEmpresas.forEach(editEmpresa => {
        editEmpresa.addEventListener('click', function(e){
            let url = this.getAttribute('data-url');
            let id = this.getAttribute('data-empresa');
            openComponentModal(url, 'Editar Empresa', 'xl', 'empresas.criarForm')
        })
    });*/
</script> --}}
@endsection
