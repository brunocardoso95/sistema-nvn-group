<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensagens', function (Blueprint $table) {
            $table->id();
            $table->string('keyId');
            $table->string('remoteJid');
            $table->boolean('fromMe');
            $table->string('text')->nullable();
            $table->unsignedBigInteger('messageTimestamp');
            $table->string('status')->nullable();
            $table->unsignedBigInteger('conexoes_id');
            $table->unsignedBigInteger('protocolo_id')->nullable();
            $table->timestamps();

            $table->index('keyId');
            $table->index('remoteJid');
            $table->index('status');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
            $table->foreign('protocolo_id')->references('id')->on('protocolo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensagens');
    }
};
