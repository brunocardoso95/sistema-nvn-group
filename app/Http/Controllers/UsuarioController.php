<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Historico;
use Illuminate\Support\Facades\Hash;
use Auth;

class UsuarioController extends Controller
{
    public function configuracoes(Request $req) {
        $data = [];
        $u = auth()->user();
        $data['usuario'] = $u;
        return view('users.configuracoes', $data);
    }

    public function trocaSenha(Request $req) {
        $id = $req->input('id');
        $senha = $req->input('antiga');
        $novaSenha = $req->input('novo');
        $confirmaSenha = $req->input('confirma');

        $usuario = User::find($id);
        $credentials = ['email'=>$usuario->email, 'password'=>$senha];

        if(isset($usuario)){
            if(Auth::validate($credentials)){
                if($novaSenha == $confirmaSenha){
                    $usuario->password = Hash::make($novaSenha);
                    if($usuario->save()){
                        return response()->json(array('error'=>false, 'data'=> array('title'=>'Atualizar Senha', 'message'=>'Sua senha foi alterada com sucesso.')), 202);
                    }else{
                        return response()->json(array('error'=>true, 'data'=> array('title'=>'Atualizar Senha', 'message'=>'Ocorreu um erro ao salvar a sua senha, tente novamente mais tarde.')), 403);
                    }
                }else{
                    return response()->json(array('error'=>true, 'data'=> array('title'=>'Atualizar Senha', 'message'=>'A senha nova é diferente da confirmação tem que ser iguais.')), 406);
                }
            }else{
                return response()->json(array('error'=>true, 'data'=> array('title'=>'Atualizar Senha', 'message'=>'A senha antiga está incorreta.')), 203);
            }
        }else{
            return response()->json(array('error'=>true, 'data'=> array('title'=>'Atualizar Senha', 'message'=>'Não encontramos o usuário.')), 403);
        }
    }

    public function index(Request $req) {
        $data = [];
        $data['usuarios'] = User::all();
        return view('users.index', $data);
    }

    public function apiUsers(Request $req, $id = 0) {
        $u = auth()->user();

        $route = 'api.init';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if($id != 0){
                $usuarios = User::where('id', $id)->first(['id','name', 'email', 'empresa_id', 'status', 'access_level', 'api']);
            }else{
                $usuarios = User::all(['id','name', 'email', 'empresa_id', 'status', 'access_level', 'api']);
            }
            if($usuarios != null){
                $response = [
                    'error' => false,
                    'usuarios' => $usuarios
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 200);
            }else{
                $response = [
                    'error' => true,
                    'usuarios' => 'Usuário não encontrado'
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 500);
            }
        }catch(\Exception $e){
            \Log::error('[method: UsuarioController@apiUsers]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'mensagem' => 'Erro na API'
                ]
            ], 500);
        }
    }

    public function apiUsersCreate(Request $req, $id = 0) {
        $u = auth()->user();

        $route = 'api.init';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if($id != 0) {
                $user = User::find($id);
            }else{
                $user = new User();
                $senha = "Mudar@123";
                $user->password = Hash::make($senha);
            }
            $user->email = $req->input('email');
            $user->empresa_id = $req->input('empresa');
            $user->status = $req->input('status', 1);
            $user->api = $req->input('api', 1);
            $user->access_level = $req->input('access_level', 0);
            if($user->save()){
                $response = [
                    'error' => false,
                    'mensagem' => "Usuário criado com sucesso!"
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 200);
            }else{
                $response = [
                    'error' => true,
                    'mensagem' => "Aconteceu um erro"
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 500);
            }


        }catch(\Exception $e){
            \Log::error('[method: UsuarioController@apiUsers]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'mensagem' => 'Erro na API'
                ]
            ], 500);
        }
    }
}
