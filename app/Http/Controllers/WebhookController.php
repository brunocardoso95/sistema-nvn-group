<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiClient\WhatsAppApi;
use App\Models\Conexoes;
use App\Models\Empresas;
use App\Models\Historico;
use App\Models\Webhook;
use Illuminate\Support\Facades\Http;
use Log;

class WebhookController extends Controller
{
    //
    public function webhook(Request $req, $session) {
        $route = 'api.webhook';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        $hist = new Historico();
        $hist->route = $route;
        $hist->path = $path;
        $hist->method = $method;
        $hist->header = json_encode($header);
        $hist->cookie = json_encode($cookie);
        $hist->request = json_encode($request);
        $hist->response = "[]";
        $hist->ip = $ip;
        $hist->save();

        try{
            $sessao = $req->input('session', null);
            $enviada = false;
            if($sessao != null){
                $con = Conexoes::where('session', $sessao)->first();
                if($con->webhook == true && $con->webhookUrl != '') {
                    $whats = new WhatsAppApi();
                    $dados = $whats->webhook($con->webhookUrl, $req->all());
                    if($dados->successful()) {
                        $enviada = true;
                    }
                }
            }

            if($con->webhook && $con->webhookUrl != ''){
                ['event'=>$event] = $req->all();
                $webhook = new Webhook();
                $webhook->type = $event;
                $webhook->url = $con->webhookUrl;
                $webhook->response = json_encode($request);
                $webhook->enviado = $enviada;
                $webhook->save();
            }

            try{
                ['event'=>$event] = $req->all();

                if($con->chatwootEnable == true){
                    switch($event){
                        case 'onmessage':
                            Http::post('https://n8n.nvngroup.com.br/webhook/picture', [$req->all(), $con]);
                        break;
                        /*case 'onpresencechanged':
                            Http::post('https://n8n.nvngroup.com.br/webhook/whatsapp-status', [$req->all(), $con]);
                        break;*/
                    }
                }
                Log::info('Webhook [method: WebhookController@webhook]: ', [$req->all()]);
            }catch(\Exception $e){}

            return true;
        }catch(\Error $e){
            Log::error('Erro ao receber Webhook [method: WebhookController@webhook]: ', [$e->getMessage()]);
            return false;
        }
    }

    public function chatwoot(Request $req, $session) {
        $whats = new WhatsAppApi();

        $route = 'api.chatwoot';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        \Log::info([$req]);

        try{
            $conexao = Conexoes::where('session', $session)->first();
            if($conexao->status < 0) {
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Sua conexão foi excluída.'
                    ]
                ];
                $hist = new Historico();
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 401);
            }
            $empresa = Empresas::find($conexao->empresa_id);
            if($empresa->status < 1) {
                $response =[
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Sua empresa se encontra suspensa, favor entrar em contato com o número: +55 21 2042-8610 para maiores detalhes.'
                    ]
                ];
                $hist = new Historico();
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 401);
            }

            try{
                ['conversation' => $conversation,'message_type' => $message_type, 'private'=>$private] = $req->all();
                ['meta'=>$meta, 'messages'=>$messages] = $conversation;

                $messages = $messages[0];

                ['sender'=>$sender] = $meta;
                ['phone_number'=>$telefone] = $sender;

                if(!$private){
                    if($message_type == "outgoing") {
                        ['sender'=>$sender2] = $req->all();
                        $nome = '';
                        if(isset($sender2) && $sender2['type'] == 'user'){
                            $nome = '*'.$sender2['name'].'*:
';
                        }
                        $messages['content'] = str_replace("\\", "", $messages['content']);
                        if(isset($messages['attachments'])){
                            $attachments = $messages['attachments'][0];
                            ['extension' => $ext] = pathinfo($attachments['data_url']);
                            if($ext == "oga") {
                                $file = file_get_contents($attachments['data_url']);
                                $dados = $whats->sendVoice($conexao->session, $conexao->token, $telefone, 'data:audio/' . $ext . ';base64,' . base64_encode($file));
                            }else {
                                $dados = $whats->sendFile($conexao->session, $conexao->token, $telefone, $attachments['data_url'], $nome.$messages['content']);
                            }
                            $dados = json_decode($dados);
                        } else {
                            $dados = $whats->sendMessage($conexao->session, $conexao->token, $telefone, $nome.$messages['content']);
                            $dados = json_decode($dados);
                        }
                    } /*elseif($message_type == "incoming") {
                        ['sender'=>$sender2] = $req->all();
                        if(isset($sender2['account']) && $messages['sender_type'] != "Contact"){
                            if(isset($messages['attachments'])){
                                $attachments = $messages['attachments'][0];
                                ['extension' => $ext] = pathinfo($attachments['data_url']);
                                if($ext == "oga") {
                                    $file = file_get_contents($attachments['data_url']);
                                    $dados = $whats->sendVoice($conexao->session, $conexao->token, $telefone, 'data:audio/' . $ext . ';base64,' . base64_encode($file));
                                }else {
                                    $dados = $whats->sendFile($conexao->session, $conexao->token, $telefone, $attachments['data_url'], $messages['content']);
                                }
                                $dados = json_decode($dados);
                            } else {
                                $dados = $whats->sendMessage($conexao->session, $conexao->token, $telefone, $messages['content']);
                                $dados = json_decode($dados);
                            }
                        }
                    }*/
                }
            }catch(\Exception $e){
                Log::error('Erro ao enviar Chatwoot [method: ApiController@chatwoot]: ', [$e->getMessage()]);
                /*$dados = $whats->chatwoot($session, $req->all());
                $dados = json_decode($dados);*/
            }

            $response =[
                'error' => false,
                'mensagem' => 'Enviado com sucesso!'
            ];
            $hist = new Historico();
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Retorno da API [method: ApiController@chatwoot]: ', [$e->getMessage()]);
            return false;
        }
    }
}
