@php
$check = Auth::check();
if($check) {
    $usuario = auth()->user();
}
@endphp

<!DOCTYPE html>
<html lang="pt-BR" dir="ltr">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>NvN Group</title>

    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicons/favicon-16x16.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicons/favicon.ico')}}">
    <link rel="manifest" href="{{asset('assets/img/favicons/manifest.json')}}">
    <meta name="msapplication-TileImage" content="{{asset('assets/img/favicons/mstile-150x150.png')}}">
    <meta name="theme-color" content="#ffffff">
    <script src="{{asset('assets/js/config.js')}}"></script>
    <script src="{{asset('vendors/simplebar/simplebar.min.js')}}"></script>

    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{ asset('vendors/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:300,400,500,600,700,800,900&amp;display=swap"
        rel="stylesheet">
    <link href="{{ asset('vendors/simplebar/simplebar.min.css') }}" rel="stylesheet">
    <link href="{{asset('assets/css/theme-rtl.min.css')}}" rel="stylesheet" id="style-rtl">
    <link href="{{asset('assets/css/theme.min.css')}}" rel="stylesheet" id="style-default">
    <link href="{{asset('assets/css/user-rtl.min.css')}}" rel="stylesheet" id="user-style-rtl">
    <link href="{{asset('assets/css/user.min.css')}}" rel="stylesheet" id="user-style-default">
    <link href="https://site-assets.fontawesome.com/releases/v6.4.0/css/all.css" rel="stylesheet">
    <script>
        var isRTL = JSON.parse(localStorage.getItem('isRTL'));
        if (isRTL) {
            var linkDefault = document.getElementById('style-default');
            var userLinkDefault = document.getElementById('user-style-default');
            linkDefault.setAttribute('disabled', true);
            userLinkDefault.setAttribute('disabled', true);
            document.querySelector('html').setAttribute('dir', 'rtl');
        } else {
            var linkRTL = document.getElementById('style-rtl');
            var userLinkRTL = document.getElementById('user-style-rtl');
            linkRTL.setAttribute('disabled', true);
            userLinkRTL.setAttribute('disabled', true);
        }

    </script>
    @yield('css')
</head>

<body>
    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
        <nav class="navbar navbar-standard navbar-expand-lg fixed-top navbar-dark" data-navbar-darken-on-scroll="data-navbar-darken-on-scroll">
            <div class="container">
                <a class="navbar-brand" href="{{route('index')}}">
                    <span class="text-white dark__text-white">NvN Group Inc</span>
                </a>
                <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarStandard" aria-controls="navbarStandard" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item d-flex align-items-center me-2">
                            <div class="nav-link theme-switch-toggle fa-icon-wait p-0">
                                <input class="form-check-input ms-0 theme-switch-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark">
                                <label class="mb-0 theme-switch-toggle-label theme-switch-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Tema light">
                                    <span class="fas fa-sun"></span>
                                </label>
                                <label class="mb-0 py-2 theme-switch-toggle-light d-lg-none" for="themeControlToggle">
                                    <span>Tema light</span>
                                </label>
                                <label class="mb-0 theme-switch-toggle-label theme-switch-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Tema dark">
                                    <span class="fas fa-moon"></span>
                                </label>
                                <label class="mb-0 py-2 theme-switch-toggle-dark d-lg-none" for="themeControlToggle">
                                    <span>Tema dark</span>
                                </label>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            @if($check)
                            <a class="nav-link" id="navbarDropdownLogin" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Seja bem vindo, {{$usuario->name}}</a>
                            @else
                            <a class="nav-link dropdown-toggle" id="navbarDropdownLogin" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
                            <div class="dropdown-menu dropdown-caret dropdown-menu-end dropdown-menu-card" aria-labelledby="navbarDropdownLogin">
                                <div class="card shadow-none navbar-card-login">
                                    <div class="card-body fs--1 p-4 fw-normal">
                                        <div class="row text-start justify-content-between align-items-center mb-2">
                                            <div class="col-auto">
                                                <h5 class="mb-0">Log in</h5>
                                            </div>
                                        </div>
                                        <form method="POST" action="{{route('loginPost')}}">
                                            @csrf
                                            <div class="mb-3">
                                                <input class="form-control" name="email" type="email" placeholder="Email" />
                                            </div>
                                            <div class="mb-3">
                                                <input class="form-control" name="password" type="password" placeholder="Senha" />
                                            </div>
                                            <div class="row flex-between-center">
                                                <div class="col-auto">
                                                    <div class="form-check mb-0">
                                                        <input class="form-check-input" type="checkbox" id="modal-checkbox" />
                                                        <label class="form-check-label mb-0" for="modal-checkbox">Lembrar-me</label>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <a class="fs--1" href="authentication/simple/forgot-password.html">Esqueceu a senha?</a>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Log in</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="py-0 overflow-hidden light" id="banner">
            <div class="bg-holder overlay"
                style="background-image:url({{asset('assets/img/generic/bg-1.jpg')}});background-position: center bottom;"></div>
            <!--/.bg-holder-->
            <div class="container">
                <div class="row flex-center pt-8 pt-lg-10 pb-lg-9 pb-xl-0">
                    <div class="col-md-11 col-lg-8 col-xl-4 pb-7 pb-xl-9 text-center text-xl-start">
                        <a class="btn btn-outline-danger mb-4 fs--1 border-2 rounded-pill" href="#!">
                            Comece hoje mesmo
                        </a>
                        <h1 class="text-white fw-light">
                            NvN Group<br />
                            <span class="typed-text fw-bold" data-typed-text='["WhatsApp","Jogos", "Sistemas para Web","Inc"]'></span>
                        </h1>
                        <p class="lead text-white opacity-75">Venha trazer mais produtividade para sua empresa com a NvN Group</p>
                        <a class="btn btn-outline-light border-2 rounded-pill btn-lg mt-4 fs-0 py-2" href="#!">Verificar produtos<span class="fas fa-play ms-2" data-fa-transform="shrink-6 down-1"></span></a>
                    </div>
                </div>
            </div><!-- end of .container-->
        </section><!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section>
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-xl-7 col-xxl-6">
                        <h1 class="fs-2 fs-sm-4 fs-md-5">Uma visão do futuro</h1>
                        <p class="lead">Estamos criando a melhor plataforma para você desfrutar de nossos serviços.</p>
                    </div>
                </div>
                <div class="row flex-center mt-8">
                    <div class="col-md col-lg-5 col-xl-4 ps-lg-6">
                        <img class="img-fluid px-6 px-md-0" src="{{asset('assets/img/icons/spot-illustrations/48.png')}}" alt="" />
                    </div>
                    <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
                        <h5 class="text-success"><span class="fab fa-whatsapp me-2"></span>WhatsApp QrCode</h5>
                        <h3>Ferramenta para WhatsApp </h3>
                        <p>Nosso painel de WhatsApp, além de você e sua equipe poder conversar com seus Leads, agora você pode mandar disparo de WhatsApp por dentro de nossa plataforma.</p>
                    </div>
                </div>
                <div class="row flex-center mt-7">
                    <div class="col-md col-lg-5 col-xl-4 pe-lg-6 order-md-2">
                        <img class="img-fluid px-6 px-md-0" src="{{asset('assets/img/icons/spot-illustrations/49.png')}}" alt="" />
                    </div>
                    <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
                        <h5 class="text-info"> <span class="fas fa-paper-plane me-2"></span>SMS</h5>
                        <h3>Shotcode & Longcode</h3>
                        <p>Oferecemos a plataforma para disparo em massa de mensagem em SMS para capturar seus leads com mais facilidade possível.</p>
                    </div>
                </div>
                <div class="row flex-center mt-7">
                    <div class="col-md col-lg-5 col-xl-4 ps-lg-6">
                        <img class="img-fluid px-6 px-md-0" src="{{asset('assets/img/icons/spot-illustrations/50.png')}}" alt="" />
                    </div>
                    <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
                        <h5 class="text-success"><span class="fa-solid fa-gamepad-alt me-2"></span>Jogos</h5>
                        <h3>Gameplay & Review</h3>
                        <p>Estamos focados em preparar a melhor plataforma de jogos online e presencias em encontros pelas praças para jogar RPG.</p>
                    </div>
                </div>
            </div><!-- end of .container-->
        </section><!-- <section> close ============================-->
        <!-- ============================================-->


        <!-- ============================================-->
        <!-- <section> begin ============================-
        <section class="bg-200 text-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="swiper-container theme-slider"
                            data-swiper='{"autoplay":true,"spaceBetween":5,"loop":true,"loopedSlides":5,"slideToClickedSlide":true}'>
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="px-5 px-sm-6">
                                        <p class="fs-sm-1 fs-md-2 fst-italic text-dark">Falcon is the best option if you
                                            are looking for a theme built with Bootstrap. On top of that, Falcon&apos;s
                                            creators and support staff are very brilliant and attentive to users&apos;
                                            needs.</p>
                                        <p class="fs-0 text-600">- Scott Tolinski, Web Developer</p><img
                                            class="w-auto mx-auto" src="../assets/img/logos/google.png" alt=""
                                            height="45" />
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="px-5 px-sm-6">
                                        <p class="fs-sm-1 fs-md-2 fst-italic text-dark">We&apos;ve become fanboys! Easy
                                            to change the modular design, great dashboard UI, enterprise-class support,
                                            fast loading time. What else do you want from a Bootstrap Theme?</p>
                                        <p class="fs-0 text-600">- Jeff Escalante, Developer</p><img
                                            class="w-auto mx-auto" src="../assets/img/logos/netflix.png" alt=""
                                            height="30" />
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="px-5 px-sm-6">
                                        <p class="fs-sm-1 fs-md-2 fst-italic text-dark">When I first saw Falcon, I was
                                            totally blown away by the care taken in the interface. It felt like
                                            something that I&apos;d really want to use and something I could see being a
                                            true modern replacement to the current class of Bootstrap themes.</p>
                                        <p class="fs-0 text-600">- Liam Martens, Designer</p><img class="w-auto mx-auto"
                                            src="../assets/img/logos/paypal.png" alt="" height="45" />
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-nav">
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end of .container-->
        </section><!-- <section> close ============================-->
        <!-- ============================================-->



        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="bg-dark pt-8 pb-4 light">
            <div class="container">
                <div class="position-absolute btn-back-to-top bg-dark"><a class="text-600" href="#banner"
                        data-bs-offset-top="0"><span class="fas fa-chevron-up" data-fa-transform="rotate-45"></span></a>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <h5 class="text-uppercase text-white opacity-85 mb-3">Nossa Missão</h5>
                        <p class="text-600"></p>
                        <div class="icon-group mt-4">
                            <a class="icon-item bg-white text-facebook" href="#!"><span class="fab fa-facebook-f"></span></a>
                            <a class="icon-item bg-white text-twitter" href="#!"><span class="fab fa-twitter"></span></a>
                            <a class="icon-item bg-white text-google-plus" href="#!"><span class="fab fa-google-plus-g"></span></a>
                            <a class="icon-item bg-white text-linkedin" href="#!"><span class="fab fa-linkedin-in"></span></a>
                            <a class="icon-item bg-white" href="#!"><span class="fab fa-medium-m"></span></a>
                        </div>
                    </div>
                    <div class="col ps-lg-6 ps-xl-8">
                        <div class="row mt-5 mt-lg-0">
                            <div class="col-6 col-md-3">
                                <h5 class="text-uppercase text-white opacity-85 mb-3">Empresa</h5>
                                <ul class="list-unstyled">
                                    <li class="mb-1"><a class="link-600" href="#!">Sobre</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Contato</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Trabalhe conosco</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Termos</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Privacidade</a></li>
                                </ul>
                            </div>
                            <div class="col-6 col-md-3">
                                <h5 class="text-uppercase text-white opacity-85 mb-3">Produtos</h5>
                                <ul class="list-unstyled">
                                    <li class="mb-1"><a class="link-600" href="#!">Onmichat</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Sites</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Portifólio</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Preços</a></li>
                                    <li class="mb-1"><a class="link-600" href="#!">Jogos</a></li>
                                </ul>
                            </div>
                            <div class="col mt-5 mt-md-0">
                                <h5 class="text-uppercase text-white opacity-85 mb-3">From the Blog</h5>
                                <ul class="list-unstyled">
                                    <li>
                                        <h5 class="fs-0 mb-0"><a class="link-600" href="#!"> Interactive graphs and
                                                charts</a></h5>
                                        <p class="text-600 opacity-50">Jan 15 &bull; 8min read </p>
                                    </li>
                                    <li>
                                        <h5 class="fs-0 mb-0"><a class="link-600" href="#!"> Lifetime free updates</a>
                                        </h5>
                                        <p class="text-600 opacity-50">Jan 5 &bull; 3min read &starf;</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end of .container-->
        </section><!-- <section> close ============================-->
        <!-- ============================================-->



        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="py-0 bg-dark light">
            <div>
                <hr class="my-0 text-600 opacity-25" />
                <div class="container py-3">
                    <div class="row justify-content-between fs--1">
                        <div class="col-12 col-sm-auto text-center">
                            <p class="mb-0 text-600 opacity-85">
                                2014-{{date("Y")}} &copy; <a class="text-white opacity-85" href="https://www.nvngroup.com.br/">NvN Group Inc</a>
                            </p>
                        </div>
                        <div class="col-12 col-sm-auto text-center">
                            <p class="mb-0 text-600 opacity-85">{{env('VERSION')}}</p>
                        </div>
                    </div>
                </div>
            </div><!-- end of .container-->
        </section><!-- <section> close ============================-->
        <!-- ============================================-->

        <div class="modal fade" id="authentication-modal" tabindex="-1" role="dialog"
            aria-labelledby="authentication-modal-label" aria-hidden="true">
            <div class="modal-dialog mt-6" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
                        <div class="position-relative z-index-1 light">
                            <h4 class="mb-0 text-white" id="authentication-modal-label">Register</h4>
                            <p class="fs--1 mb-0 text-white">Please create your free Falcon account</p>
                        </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body py-4 px-5">
                        <form>
                            <div class="mb-3"><label class="form-label" for="modal-auth-name">Name</label><input
                                    class="form-control" type="text" autocomplete="on" id="modal-auth-name" /></div>
                            <div class="mb-3"><label class="form-label" for="modal-auth-email">Email
                                    address</label><input class="form-control" type="email" autocomplete="on"
                                    id="modal-auth-email" /></div>
                            <div class="row gx-2">
                                <div class="mb-3 col-sm-6"><label class="form-label"
                                        for="modal-auth-password">Password</label><input class="form-control"
                                        type="password" autocomplete="on" id="modal-auth-password" /></div>
                                <div class="mb-3 col-sm-6"><label class="form-label"
                                        for="modal-auth-confirm-password">Confirm Password</label><input
                                        class="form-control" type="password" autocomplete="on"
                                        id="modal-auth-confirm-password" /></div>
                            </div>
                            <div class="form-check"><input class="form-check-input" type="checkbox"
                                    id="modal-auth-register-checkbox" /><label class="form-label"
                                    for="modal-auth-register-checkbox">I accept the <a href="#!">terms </a>and <a
                                        href="#!">privacy policy</a></label></div>
                            <div class="mb-3"><button class="btn btn-primary d-block w-100 mt-3" type="submit"
                                    name="submit">Register</button></div>
                        </form>
                        <div class="position-relative mt-5">
                            <hr />
                            <div class="divider-content-center">or register with</div>
                        </div>
                        <div class="row g-2 mt-2">
                            <div class="col-sm-6"><a class="btn btn-outline-google-plus btn-sm d-block w-100"
                                    href="#"><span class="fab fa-google-plus-g me-2" data-fa-transform="grow-8"></span>
                                    google</a></div>
                            <div class="col-sm-6"><a class="btn btn-outline-facebook btn-sm d-block w-100"
                                    href="#"><span class="fab fa-facebook-square me-2"
                                        data-fa-transform="grow-8"></span> facebook</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->

    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="{{asset('vendors/popper/popper.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/anchorjs/anchor.min.js')}}"></script>
    <script src="{{asset('vendors/is/is.min.js')}}"></script>
    <script src="{{asset('vendors/swiper/swiper-bundle.min.js')}}"> </script>
    <script src="{{asset('vendors/typed.js/typed.js')}}"></script>
    <script src="{{asset('vendors/fontawesome/all.min.js')}}"></script>
    <script src="{{asset('vendors/lodash/lodash.min.js')}}"></script>
    <script src="{{asset('vendors/polyfill.io/v3/polyfill.min58be.js?features=window.scroll')}}"></script>
    <script src="{{asset('vendors/list.js/list.min.js')}}"></script>
    <script src="{{asset('assets/js/theme.js')}}"></script>
    @yield('js')
</body>
</html>
