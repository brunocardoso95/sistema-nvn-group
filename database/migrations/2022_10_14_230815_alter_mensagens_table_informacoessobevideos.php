<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mensagens', function (Blueprint $table) {
            $table->text('streamingSidecar')->nullable();
            $table->unsignedBigInteger('seconds')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mensagens', function (Blueprint $table) {
            $table->dropColumn('streamingSidecar');
            $table->dropColumn('seconds');
        });
    }
};
