<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \App\Models\Historico;

class IsAdminApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $req, Closure $next)
    {
        $u = auth()->user();
        $route = 'IsAdminApi';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        if($u->isAdmin()){
            return $next($req);
        }else{
            $response = [
                'error' => true,
                'data' => [
                    'mensagem' => 'Você não tem permissão de Administrador do Sistema.',
                ],
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, 401);
        }
    }
}
