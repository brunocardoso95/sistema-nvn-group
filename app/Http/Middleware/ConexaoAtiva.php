<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \App\Models\Conexoes;
use \App\Models\Historico;

class ConexaoAtiva
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $req, Closure $next)
    {
        $u = auth()->user();
        $route = 'ConexaoAtiva';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        $con = Conexoes::where('empresa_id', $u->empresa_id)->where('status', '>=', 0)->where('session',$req->input('canal'))->first();
        if($con == null){
            $response = [
                'error' => true,
                'data' => [
                    'mensagem' => 'Preencha o {canal} corretamente.',
                ],
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, 406);
        }
        $req['con'] = $con;
        return $next($req);
    }
}
