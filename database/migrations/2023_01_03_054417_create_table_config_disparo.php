<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_disparo', function (Blueprint $table) {
            $table->id();
            $table->string('file');
            $table->integer('telefone');
            $table->integer('identificador')->nullable();
            $table->integer('nome')->nullable();
            $table->text('mensagem')->nullable();
            $table->integer('status');
            $table->unsignedBigInteger('conexoes_id');
            $table->timestamps();

            $table->index('telefone');
            $table->index('status');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_disparo');
    }
};
