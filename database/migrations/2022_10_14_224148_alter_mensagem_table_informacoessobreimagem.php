<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mensagens', function (Blueprint $table) {
            $table->string('url')->nullable();
            $table->text('jpegthumbnail')->nullable();
            $table->string('directpath')->nullable();
            $table->string('mimetype')->nullable();
            $table->string('filesha256')->nullable();
            $table->string('fileencsha256')->nullable();
            $table->string('midqualityfilesha256')->nullable();
            $table->unsignedBigInteger('filelength')->nullable();
            $table->unsignedBigInteger('height')->nullable();
            $table->unsignedBigInteger('width')->nullable();
            $table->string('mediakey')->nullable();
            $table->unsignedBigInteger('mediakeytimestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mensagens', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('jpegthumbnail');
            $table->dropColumn('directpath');
            $table->dropColumn('mimetype');
            $table->dropColumn('filesha256');
            $table->dropColumn('fileencsha256');
            $table->dropColumn('midqualityfilesha256');
            $table->dropColumn('filelength');
            $table->dropColumn('height');
            $table->dropColumn('width');
            $table->dropColumn('mediakey');
            $table->dropColumn('mediakeytimestamp');
        });
    }
};
