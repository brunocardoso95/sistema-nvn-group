<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    use HasFactory;
    public $table = "webhooks";
    public $timestamps = true;
    public $incrementing = true;

    protected $fillable = [
        'type',
        'url',
        'response',
        'enviado',
    ];
}
