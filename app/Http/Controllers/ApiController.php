<?php

namespace App\Http\Controllers;

use App\ApiClient\WhatsAppApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conexoes;
use App\Models\Empresas;
use App\Models\Historico;

class ApiController extends Controller
{
    public function status(Request $req) {
        $whats = new WhatsAppApi();

        $route = 'api.status';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->status();
            $dados = json_decode($dados);
            $response = [
                'error' => false,
                'status' => $dados
            ];
            $hist = new Historico();
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            \Log::info('[method: ApiController@status]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'mensagem' => 'Desconectado'
                ]
            ]);
        }
    }
}
