<?php

namespace App\ApiClient;

use Illuminate\Support\Facades\Http;

Class ApiClient {
    public function get($link, $token = false){
        if($token){
            return Http::withToken($token)->get($link);
        }else{
            return Http::get($link);
        }
    }

    public function post($link, $token = false, $array = []){
        if($token){
            return Http::withToken($token)->post($link, $array);
        }else{
            return Http::post($link, $array);
        }
    }

    public function put($link, $token = false, $array = []){
        if($token){
            return Http::withToken($token)->put($link, $array);
        }else{
            return Http::put($link, $array);
        }
    }

    public function delete($link, $token = false, $array = []){
        if($token){
            return Http::withToken($token)->delete($link, $array);
        }else{
            return Http::delete($link);
        }
    }
}
