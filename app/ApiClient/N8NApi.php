<?php

namespace App\ApiClient;

class N8NApi extends ApiClient {
    public $urlN8n;

    public function __construct() {
        $this->urlN8n = env('URL_N8N');
    }
}
