<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('remoteJid', 30);
            $table->string('pushName')->nullable();
            $table->bigInteger('numero')->nullable();
            $table->string('nome')->nullable();
            $table->string('presences')->nullable();
            $table->string('ultimamensagem', 30)->nullable();
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('conexoes_id');
            $table->timestamps();

            $table->index('numero');
            $table->index('remoteJid');
            $table->index('conexoes_id');
            $table->index('ultimamensagem');

            $table->foreign('conexoes_id')->references('id')->on('conexoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
};
