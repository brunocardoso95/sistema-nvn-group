<form method="POST" action="{{route('conexao.createPost')}}@if($c->id!=null){{$c->id}}@endif">
    @csrf
    <input type="hidden" name="id" id="id" value="{{$c->id}}">
    <div class="row">
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="nome" id='nome' placeholder="Nome da Conexão" value="{{$c->nome}}" required>
                <label for="nome">Nome da Conexão</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="webhookUrl" id="webhookUrl" placeholder="URL do Webhook" value="{{$c->webhookUrl}}">
                <label for="webhookUrl">URL do Webhook</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="chatwootUrl" id="chatwootUrl" placeholder="URL do Chatwoot" value="{{$c->chatwootUrl}}">
                <label for="chatwootUrl">URL do Chatwoot</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="text" class="form-control form-control-lg" name="chatwootToken" id='chatwootToken' placeholder="Token do Chatwoot" value="{{$c->chatwootToken}}">
                <label for="chatwootToken">Token do Chatwoot</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="number" class="form-control form-control-lg" name="chatwootAccount" id='chatwootAccount' placeholder="ID da Conta do Chatwoot" value="{{$c->chatwootAccount}}">
                <label for="chatwootAccount">ID da Conta do Chatwoot</label>
            </div>
        </div>
        <div class="col-lg-4 mb-2">
            <div class="form-floating">
                <input type="number" class="form-control form-control-lg" name="chatwootInbox" id="chatwootInbox" placeholder="ID da Inbox do Chatwoot" value="{{$c->chatwootInbox}}">
                <label for="chatwootInbox">ID da Inbox do Chatwoot</label>
            </div>
        </div>
    </div>
</form>

<script>
</script>
