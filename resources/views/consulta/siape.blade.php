@extends('layouts.system')

@section('content')
<div class="container-fluid">
    <nav style="--falcon-breadcrumb-divider: '»';" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Campanhas</li>
        </ol>
    </nav>
    <div class="card w-100 mt-2">
        <div class="card-body">
            <div class="search-box" data-list='{"valueNames":["title"]}'>
                <form class="position-relative" data-bs-toggle="search" data-bs-display="static">
                    <input class="form-control search-input fuzzy-search" type="search" placeholder="Procurar..." aria-label="Search" />
                    <span class="fas fa-search search-box-icon"></span>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@endsection

@section('js')
@endsection
