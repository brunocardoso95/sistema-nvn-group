<?php

namespace App\Http\Controllers;
use App\ApiClient\WhatsAppApi;
use App\Models\Historico;

use Illuminate\Http\Request;

class GrupoController extends Controller
{
    //
    public function createGroup(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.createGroup';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $nome = $req->input('nome', false);
            if(!$nome){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {nome} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->createGroup($req['con']->session, $req['con']->token, $nome);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@createGroup]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function joinGroupByCode(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.joinGroupByCode';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $codigo = $req->input('codigo', false);
            if(!$codigo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {codigo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->joinCode($req['con']->session, $req['con']->token, "https://chat.whatsapp.com/$codigo");
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@joinGroupByCode]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function addParticipantGroup(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.addParticipantGroup';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            $idgrupo = $req->input('idgrupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idgrupo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idgrupo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->addParticipantGroup($req['con']->session, $req['con']->token, $telefone, $idgrupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@addParticipantGroup]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function removeParticipantGroup(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.removeParticipantGroup';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            $idgrupo = $req->input('idgrupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idgrupo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idgrupo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->removeParticipantGroup($req['con']->session, $req['con']->token, $telefone, $idgrupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@removeParticipantGroup]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function promoteParticipantGroup(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.promoteParticipantGroup';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            $idgrupo = $req->input('idgrupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idgrupo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idgrupo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->promoteParticipantGroup($req['con']->session, $req['con']->token, $telefone, $idgrupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@promoteParticipantGroup]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function demoteParticipantGroup(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.demoteParticipantGroup';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $telefone = $req->input('telefone', false);
            $idgrupo = $req->input('idgrupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idgrupo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idgrupo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->demoteParticipantGroup($req['con']->session, $req['con']->token, $telefone, $idgrupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@demoteParticipantGroup]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function getAllGroups(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.getAllGroups';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allGroups($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'alert' => 'Deprecated: This function [todos-grupos] is deprecated in favor of the [listar-chats] function. Please update your code accordingly',
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@getAllGroups]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function getAllBroadcastList(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.getAllBroadcastList';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $dados = $whats->allBroadcastList($req['con']->session, $req['con']->token);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@getAllBroadcastList]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function groupAdmins(Request $req, $idgrupo = false) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.groupAdmins';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            if(!$idgrupo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idgrupo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->groupAdmins($req['con']->session, $req['con']->token, $idgrupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@groupAdmins]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function groupInfoFromInviteLink(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'group.groupInfoFromInviteLink';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];
        try{
            $codigo = $req->input('codigo', false);
            if(!$codigo){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {codigo} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $dados = $whats->groupInfoFromInviteLink($req['con']->session, $req['con']->token, "https://chat.whatsapp.com/$codigo");
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $status = 200;

            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response, $status);
        }catch(\Exception $e){
            \Log::error('Erro na API [method: GrupoController@groupInfoFromInviteLink]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }
}
