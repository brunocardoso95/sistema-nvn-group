@extends('layouts.system')

@section('content')
<div class="container-fluid">
    <nav style="--falcon-breadcrumb-divider: '»';" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Campanhas</li>
          <li class="breadcrumb-item"><a href="{{route('campanhas.create')}}" class="me-1"><i class="align-middle fa-solid fa-add"></i></a></li>
        </ol>
    </nav>
    <div class="card w-100 mt-2">
        <div class="card-body">

        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    /*<% if(data.campanhas.length > 0) { %>
        <% data.campanhas.forEach(campanhas => { %>
            document.getElementById('trash_<%=campanhas._id%>').addEventListener('click', function(){
                const dados = 'idCampanha=<%=campanhas._id%>'
                const trashAjax_<%=campanhas._id%> = new XMLHttpRequest();
                trashAjax_<%=campanhas._id%>.open('POST', '/web/trashCampanha', true);
                trashAjax_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                trashAjax_<%=campanhas._id%>.send(dados)
                trashAjax_<%=campanhas._id%>.onreadystatechange = function(){
                    if(trashAjax_<%=campanhas._id%>.readyState == 4 && trashAjax_<%=campanhas._id%>.status == 200){
                        const resposta = trashAjax_<%=campanhas._id%>.responseText;
                        try{
                            document.getElementById('play_<%=campanhas._id%>').classList.add('d-none');
                        }catch(e){}
                    }
                }
            })

            <% switch(campanhas.status){
                case 1:
            %>
                    document.getElementById('play_<%=campanhas._id%>').addEventListener('click', function(){
                        const dados = 'idCampanha=<%=campanhas._id%>'
                        const playAjax_<%=campanhas._id%> = new XMLHttpRequest();
                        playAjax_<%=campanhas._id%>.open('POST', '/web/playCampanha', true);
                        playAjax_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                        playAjax_<%=campanhas._id%>.send(dados)
                        playAjax_<%=campanhas._id%>.onreadystatechange = function(){
                            if(playAjax_<%=campanhas._id%>.readyState == 4 && playAjax_<%=campanhas._id%>.status == 200){
                                const resposta = playAjax_<%=campanhas._id%>.responseText;
                            }
                        }
                    })
            <%
                break;
                case 2:
            %>
                    document.getElementById('pause_<%=campanhas._id%>').addEventListener('click', function(){
                        const dados = 'idCampanha=<%=campanhas._id%>'
                        const playAjax_<%=campanhas._id%> = new XMLHttpRequest();
                        playAjax_<%=campanhas._id%>.open('POST', '/web/pauseCampanha', true);
                        playAjax_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                        playAjax_<%=campanhas._id%>.send(dados)
                        playAjax_<%=campanhas._id%>.onreadystatechange = function(){
                            if(playAjax_<%=campanhas._id%>.readyState == 4 && playAjax_<%=campanhas._id%>.status == 200){
                                const resposta = playAjax_<%=campanhas._id%>.responseText;
                            }
                        }
                    })
            <%
                break;
                case 3:
            %>
                    document.getElementById('play_<%=campanhas._id%>').addEventListener('click', function(){
                        const dados = 'idCampanha=<%=campanhas._id%>'
                        const playAjax_<%=campanhas._id%> = new XMLHttpRequest();
                        playAjax_<%=campanhas._id%>.open('POST', '/web/playCampanha', true);
                        playAjax_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                        playAjax_<%=campanhas._id%>.send(dados)
                        playAjax_<%=campanhas._id%>.onreadystatechange = function(){
                            if(playAjax_<%=campanhas._id%>.readyState == 4 && playAjax_<%=campanhas._id%>.status == 200){
                                const resposta = playAjax_<%=campanhas._id%>.responseText;
                            }
                        }
                    })
            <%
                break;
                case 4:
            %>
                document.getElementById('download_<%=campanhas._id%>_true').addEventListener('click', function(){
                    const dados = 'download=<%=campanhas._id%>&status=true'
                    const download_<%=campanhas._id%> = new XMLHttpRequest();
                    download_<%=campanhas._id%>.open('POST', '/web/downloadCampanha', true);
                    download_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                    download_<%=campanhas._id%>.send(dados)
                    download_<%=campanhas._id%>.onreadystatechange = function(){
                        if(download_<%=campanhas._id%>.readyState == 4 && download_<%=campanhas._id%>.status == 200){
                            const resposta = JSON.parse(download_<%=campanhas._id%>.responseText);
                            downloadCSVFromJson('<%=campanhas.campanha%>_Result.csv',resposta)
                        }
                    }
                })

                document.getElementById('download_<%=campanhas._id%>_false').addEventListener('click', function(){
                    const dados = 'download=<%=campanhas._id%>&status=false'
                    const download_<%=campanhas._id%> = new XMLHttpRequest();
                    download_<%=campanhas._id%>.open('POST', '/web/downloadCampanha', true);
                    download_<%=campanhas._id%>.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                    download_<%=campanhas._id%>.send(dados)
                    download_<%=campanhas._id%>.onreadystatechange = function(){
                        if(download_<%=campanhas._id%>.readyState == 4 && download_<%=campanhas._id%>.status == 200){
                            const resposta = JSON.parse(download_<%=campanhas._id%>.responseText);
                            downloadCSVFromJson('<%=campanhas.campanha%>_NotOk.csv',resposta)
                        }
                    }
                })
            <% break;
            } %>
        <% }) %>
    <% } %>*/
</script>
@endsection
