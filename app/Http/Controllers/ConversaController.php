<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiClient\WhatsAppApi;
use App\Models\Historico;
use Log;

class ConversaController extends Controller
{
    //
    public function enviarMensagem(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarMensagem';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $mensagem = $req->input('mensagem', false);
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$mensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {mensagem} corretamente.',
                    ]];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendMessage($req['con']->session, $req['con']->token, $telefone, $mensagem, $grupo);
            $dados = json_decode($dados);
            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarMensagem]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarFileBase64(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarFileBase64';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $base64 = $req->input('base64', false);
            $filename = $req->input('filename', 'Arquivo');
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            if(!$base64){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {base64} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendFileBase64($req['con']->session, $req['con']->token, $telefone, $base64, $filename, $grupo);
            $dados = json_decode($dados);
            if($dados->status == "Error"){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => $dados->message,
                        'error' => $dados->error,
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarFileBase64]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarImageBase64(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarImageBase64';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $base64 = $req->input('base64', false);
            $filename = $req->input('filename', 'Arquivo');
            $mensagem = $req->input('mensagem');
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            if(!$base64){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {base64} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendImage($req['con']->session, $req['con']->token, $telefone, $base64, $mensagem, $filename, $grupo);
            $dados = json_decode($dados);
            if($dados->status == "Error"){
                $response = [
                    'error' => true,
                    'dados' => $dados
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $response = [
                'error' => false,
                'data' => [
                    'id' => $dados->response[0]->id,
                    'ack' => $dados->response[0]->ack,
                    'sendMsgResult' => $dados->response[0]->sendMsgResult,
                ],
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarImageBase64]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarAudioBase64(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarAudioBase64';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $base64 = $req->input('base64', false);
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            if(!$base64){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {base64} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendVoice($req['con']->session, $req['con']->token, $telefone, $base64, $grupo);
            $dados = json_decode($dados);
            if($dados->status == "Error"){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => $dados->message,
                        'error' => $dados->error,
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarAudioBase64]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarReply(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarReply';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $mensagem = $req->input('mensagem', false);
            $idmensagem = $req->input('idmensagem', false);
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$mensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {mensagem} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idmensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idmensagem} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendReply($req['con']->session, $req['con']->token, $telefone, $mensagem, $idmensagem, $grupo);
            $dados = json_decode($dados);


            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarButtons(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarButtons';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $titulo = $req->input('titulo', '');
            $mensagem = $req->input('mensagem', '');
            $rodape = $req->input('rodape', '');
            $usarTemplate = $req->input('usarTemplate', true);
            $botoes = $req->input('botoes', []);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(count($botoes) == 0){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {botoes} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $i = 1;
            $botao = [];
            foreach($botoes as $b){
                $info = [];
                $info['id'] = strval($i);
                if(isset($b["texto"])) $info['text'] = $b["texto"];
                if(isset($b["url"])) $info['url'] = $b["url"];
                if(isset($b["telefone"])) $info['phoneNumber'] = $b["telefone"];
                array_push($botao, $info);
                $i++;
            }

            $dados = $whats->sendButtons($req['con']->session, $req['con']->token, $telefone, $botao, $usarTemplate, $titulo, $mensagem, $rodape);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => [$dados]
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);}
    }

    public function encaminharMensagem(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.encaminharMensagem';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $idmensagem = $req->input('idmensagem', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idmensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idmensagem} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->forwardMessage($req['con']->session, $req['con']->token, $telefone, $idmensagem);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarContato(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarContato';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $idcontato = $req->input('idcontato', false);
            $nome = $req->input('nome');
            $grupo = $req->input('grupo', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$idcontato){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {idcontato} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->contactVcard($req['con']->session, $req['con']->token, $telefone, $idcontato, $nome, $grupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarLink(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarLink';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $url = $req->input('url', false);
            $mensagem = $req->input('mensagem', false);
            if(!$telefone){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$url){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {url} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(!$mensagem){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {mensagem} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendLinkPreview($req['con']->session, $req['con']->token, $telefone, $url, $mensagem);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarEnquete(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarLink';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $nome = $req->input('nome', false);
            $escolhas = $req->input('escolhas', []);
            $selecionaveis = $req->input('selecionaveis');
            $grupo = $req->input('grupo', false);
            if(!$telefone)
            return response()->json([
                'error' => true,
                'data' => [
                    'mensagem' => 'Preencha o {telefone} corretamente.',
                ],
            ], 400);
            if(!$nome){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {nome} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }
            if(count($escolhas) == 0){
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {escolhas} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendPollMessage($req['con']->session, $req['con']->token, $telefone, $nome, $escolhas, $selecionaveis, $grupo);
            $dados = json_decode($dados);

            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarReply]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarLocalizacao(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarLocalizacao';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try {
            $telefone = $req->input('telefone', false);
            $lat = $req->input('lat', 0);
            $lng = $req->input('lng', 0);
            $titulo = $req->input('titulo', null);
            $endereco = $req->input('endereco', null);

            if(!$telefone) {
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendLocation($req['con']->session, $req['con']->token, $telefone, $lat, $lng, $titulo, $endereco);
            $dados = json_decode($dados);
            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);

        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarLocalizacao]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }

    public function enviarSticker(Request $req) {
        $whats = new WhatsAppApi();
        $u = auth()->user();

        $route = 'chat.enviarSticker';
        $path = $req->path();
        $method = $req->method();
        $header = $req->header();
        $cookie = $req->cookie();
        $request = $req->all();
        $ip = $header["cf-connecting-ip"][0];

        try{
            $telefone = $req->input('telefone', false);
            $arquivo = $req->input('arquivo', false);
            $grupo = $req->input('grupo', false);

            if(!$telefone) {
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            if(!$arquivo) {
                $response = [
                    'error' => true,
                    'data' => [
                        'mensagem' => 'Preencha o {telefone} corretamente.',
                    ],
                ];
                $hist = new Historico();
                $hist->usuario_id = $u->id;
                $hist->route = $route;
                $hist->path = $path;
                $hist->method = $method;
                $hist->header = json_encode($header);
                $hist->cookie = json_encode($cookie);
                $hist->request = json_encode($request);
                $hist->response = json_encode($response);
                $hist->ip = $ip;
                $hist->save();
                return response()->json($response, 406);
            }

            $dados = $whats->sendSticker($req['con']->session, $req['con']->token, $telefone, $arquivo, $grupo);
            $dados = json_decode($dados);
            $response = [
                'error' => false,
                'dados' => $dados
            ];
            $hist = new Historico();
            $hist->usuario_id = $u->id;
            $hist->route = $route;
            $hist->path = $path;
            $hist->method = $method;
            $hist->header = json_encode($header);
            $hist->cookie = json_encode($cookie);
            $hist->request = json_encode($request);
            $hist->response = json_encode($response);
            $hist->ip = $ip;
            $hist->save();
            return response()->json($response);
        }catch(\Exception $e){
            Log::error('Erro na API [method: ConversaController@enviarSticker]: ', [$e->getMessage()]);
            return response()->json([
                'error' => true,
                'data' => [
                    'message' => 'Internal Server Error',
                    'mensagem' => 'Erro na API',
                ],
            ], 500);
        }
    }
}
