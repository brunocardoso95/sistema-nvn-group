@extends('layouts.system')

@section('content')
<div class="row g-3 mb-3">
    <div class="col-xxl-6 col-lg-12">
        <div class="card h-100">
            <div class="bg-holder bg-card" style="background-image:url({{ asset('assets/img/icons/spot-illustrations/corner-3.png') }});"></div>
            <!--/.bg-holder-->
            <div class="card-header z-index-1">
                <h5 class="text-primary">Bem vindo ao Sistemas NvN Group</h5>
                <h6 class="text-600">Aqui temos os links rápidos, onde você pode configurar para sua conta.</h6>
            </div>
            <div class="card-body z-index-1">
                <div class="row g-2 h-100 align-items-end">
                    <div class="col-sm-6 col-md-5">
                        <div class="d-flex position-relative">
                            <div class="icon-item icon-item-sm border rounded-3 shadow-none me-2">
                                <span class="fas fa-comments text-primary"></span>
                            </div>
                            <div class="flex-1">
                                <a class="stretched-link text-800" href="#!">
                                    <h6 class="mb-0">Chat</h6>
                                </a>
                                <p class="mb-0 fs--2 text-500">Entrar na tela de conversão com os clientes.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-5">
                        <div class="d-flex position-relative">
                            <div class="icon-item icon-item-sm border rounded-3 shadow-none me-2">
                                <span class="fas fa-crown text-warning"></span>
                            </div>
                            <div class="flex-1">
                                <a class="stretched-link text-800" href="#!">
                                    <h6 class="mb-0">Upgrade</h6>
                                </a>
                                <p class="mb-0 fs--2 text-500">Aumente seu plano</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-5">
                        <div class="d-flex position-relative">
                            <div class="icon-item icon-item-sm border rounded-3 shadow-none me-2">
                                <span class="fas fa-video text-success"></span></div>
                            <div class="flex-1">
                                <a class="stretched-link text-800" href="#!">
                                    <h6 class="mb-0">Marcar treinamento por vídeo</h6>
                                </a>
                                <p class="mb-0 fs--2 text-500">Aqui você pode marcar uma reunião com nossa equipe técnica para entender o sistema</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-5">
                        <div class="d-flex position-relative">
                            <div class="icon-item icon-item-sm border rounded-3 shadow-none me-2">
                                <span class="fas fa-user text-info"></span>
                            </div>
                            <div class="flex-1">
                                <a class="stretched-link text-800" href="#!">
                                    <h6 class="mb-0">Atividades dos Usuários</h6>
                                </a>
                                <p class="mb-0 fs--2 text-500">Monitorar e supervisionar seus Agentes</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-3 col-md-6">
        <div class="card h-100">
            <div class="card-header d-flex flex-between-center">
                <h5 class="mb-0">Campanhas</h5>
            </div>
            <div class="card-body">
                <p class="fs--1 text-600">Verifica a quantidade e o status de como está suas campanhas</p>
                <div class="progress mb-3 rounded-pill" style="height: 6px;">
                    <div class="progress-bar bg-progress-gradient rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <p class="mb-0 text-primary">0% completado</p>
            </div>
        </div>
    </div>
    <div class="col-xxl-3 col-md-6">
        <div class="card h-100">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col">
                        <p class="mb-1 fs--2 text-500">Agendamento</p>
                        <h5 class="text-primary fs-0">Treinamento</h5>
                    </div>
                    <div class="col-auto">
                        <div class="bg-soft-primary px-3 py-3 rounded-circle text-center"
                            style="width:60px;height:60px;">
                            <h5 class="text-primary mb-0 d-flex flex-column mt-n1">
                                <span>{{date('d')}}</span>
                                <small class="text-primary fs--2 lh-1">{{strtoupper(date('M'))}}</small>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body d-flex align-items-end">
                <div class="row g-3 justify-content-between">
                    <div class="col-10 mt-0">
                        <p class="fs--1 text-600 mb-0"></p>
                    </div>
                    @if($link)
                    <div class="col-auto"><button class="btn btn-success w-100 fs--1" type="button">
                        <span class="fas fa-video me-2"></span>Join meeting</button>
                    </div>
                    <div class="col-auto ps-0">
                        <div class="avatar-group avatar-group-dense">
                            <div class="avatar avatar-xl border border-3 border-light rounded-circle">
                                <img class="rounded-circle" src="{{ asset('assets/img/team/1-thumb.png') }}" alt="" />
                            </div>
                            <div class="avatar avatar-xl border border-3 border-light rounded-circle">
                                <img class="rounded-circle" src="{{ asset('assets/img/team/2-thumb.png') }}" alt="" />
                            </div>
                            <div class="avatar avatar-xl border border-3 border-light rounded-circle">
                                <img class="rounded-circle" src="{{ asset('assets/img/team/3-thumb.png') }}" alt="" />
                            </div>
                            <div class="avatar avatar-xl border border-3 border-light rounded-circle">
                                <div class="avatar-name rounded-circle "><span>+50</span></div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row g-3 mb-3">
    <div class="col-xxl-6 col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="row g-3 mb-3">
                    <div class="col-sm-6">
                        <div class="card overflow-hidden" style="min-width: 12rem">
                            <div class="bg-holder bg-card" style="background-image:url({{ asset('assets/img/icons/spot-illustrations/corner-2.png') }});"></div>
                            <!--/.bg-holder-->
                            <div class="card-body position-relative">
                                <h6>Conversas abertas<span class="badge badge-soft-info rounded-pill ms-2">0.00%</span></h6>
                                <div class="display-4 fs-4 mb-2 fw-normal font-sans-serif text-info" data-countup='{"endValue":0,"decimalPlaces":0,"suffix":""}'>0
                                </div>
                                <a class="fw-semi-bold fs--1 text-nowrap" href="../app/e-commerce/orders/order-list.html">
                                    Todas as Conversas <span class="fas fa-angle-right ms-1" data-fa-transform="down-1"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card overflow-hidden" style="min-width: 12rem">
                            <div class="bg-holder bg-card"
                                style="background-image:url({{ asset('assets/img/icons/spot-illustrations/corner-1.png') }});">
                            </div>
                            <!--/.bg-holder-->
                            <div class="card-body position-relative">
                                <h6>Contatos novos<span class="badge badge-soft-warning rounded-pill ms-2">0.00%</span></h6>
                                <div class="display-4 fs-4 mb-2 fw-normal font-sans-serif text-warning" data-countup='{"endValue":0,"decimalPlaces":0,"suffix":""}'>0</div>
                                <a class="fw-semi-bold fs--1 text-nowrap" href="../app/e-commerce/customers.html">Todos os Contatos<span class="fas fa-angle-right ms-1" data-fa-transform="down-1"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="row flex-between-center g-card">
                            <div class="col-auto">
                                <h6 class="mb-0">Report for this week</h6>
                            </div>
                            <div class="col-auto d-flex">
                                <div class="btn btn-sm btn-text d-flex align-items-center p-0 me-3 shadow-none" id="echart-bar-report-for-this-week-option-1">
                                    <span class="fas fa-circle text-primary fs--2 me-1"></span><span class="text">This Week</span>
                                </div>
                                <div class="btn btn-sm btn-text d-flex align-items-center p-0 shadow-none" id="echart-bar-report-for-this-week-option-2">
                                    <span class="fas fa-circle text-300 fs--2 me-1"></span>
                                    <span class="text">Last Week</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-0">
                        <!-- Find the JS file for the following chart at: src/js/charts/echarts/report-for-this-week.js-->
                        <!-- If you are not using gulp based workflow, you can find the transpiled code at: public/assets/js/theme.js-->
                        <div class="echart-bar-report-for-this-week" data-echart-responsive="true" data-chart="{&quot;option1&quot;:&quot;echart-bar-report-for-this-week-option-1&quot;,&quot;option2&quot;:&quot;echart-bar-report-for-this-week-option-2&quot;}">
                        </div>
                    </div>
                    <div class="card-footer bg-light p-0">
                        <!--<a class="btn btn-sm btn-link d-block w-100 py-2" href="#!">See all projects<span class="fas fa-chevron-right ms-1 fs--2"></span></a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 order-xxl-3">
        <div class="card" id="runningProjectTable"
            data-list="{&quot;valueNames&quot;:[&quot;projects&quot;,&quot;worked&quot;,&quot;time&quot;,&quot;date&quot;]}">
            <div class="card-header">
                <h6 class="mb-0">Campanhas Executando</h6>
            </div>
            <div class="card-body p-0">
                <div class="scrollbar">
                    <table class="table mb-0 table-borderless fs--2 border-200 overflow-hidden table-running-project">
                        <thead class="bg-light">
                            <tr class="text-800">
                                <th class="sort" data-sort="projects">Projects</th>
                                <th class="sort" data-sort="time">Progress</th>
                                <th class="sort text-center" data-sort="worked"> Worked</th>
                                <th class="sort text-center" data-sort="date">Due Date</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center position-relative">
                                        <div class="avatar avatar-xl">
                                            <div class="avatar-name rounded-circle text-primary bg-soft-primary fs-0">
                                                <span>C</span></div>
                                        </div>
                                        <div class="flex-1 ms-3">
                                            <h6 class="mb-0 fw-semi-bold"><a class="stretched-link text-900"
                                                    href="../pages/user/profile.html">CRM dashboard design</a></h6>
                                            <p class="text-500 fs--2 mb-0">Falcon</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    <div class="progress rounded-3 worked" style="height: 6px;">
                                        <div class="progress-bar bg-progress-gradient rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="0.0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td class="align-middle text-center time">
                                    <p class="fs--1 mb-0 fw-semi-bold">{{date('H:i:s')}}</p>
                                </td>
                                <td class="align-middle text-center date">
                                    <p class="fs--1 mb-0 fw-semi-bold">{{date('d/m/Y')}}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-light py-0 text-center">
                <a class="btn btn-sm btn-link py-2" href="{{route('campanha')}}">
                    Veja todas as campanhas
                    <span class="fas fa-chevron-right ms-1 fs--2"></span>
                </a>
            </div>
        </div>
    </div>--}}
    @endsection
