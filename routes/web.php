<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\WhatsappController;
use App\Http\Controllers\ConsultaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                                             [SiteController::class, 'login'])->name('index');
Route::get('/login',                                        [SiteController::class, 'login'])->name('login');
Route::post('/login',                                       [SiteController::class, 'loginPost'])->name('loginPost');
Route::get('/sair',                                         [SystemController::class, 'sair'])->name('sair');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard',                                [SystemController::class, 'index'])->name('dashboard');

    //WhatsApp
    /*Route::get('/campanha',                               [SystemController::class, 'campanha'])->name('campanha');*/
    Route::get('/conexoes',                                 [SystemController::class, 'conexoes'])->name('conexoes');
    Route::get('/conexoes/status',                          [SystemController::class, 'conexoesStatus'])->name('conexoes.status');
    Route::get('/conexoes/criar/{id?}',                     [SystemController::class, 'createConexao'])->name('conexao.create');
    Route::post('/conexoes/criar/{id?}',                    [SystemController::class, 'createConexao'])->name('conexao.createPost');
    Route::get('/chat',                                     [WhatsappController::class, 'chat'])->name('chat');

    //Usuário
    Route::get('/usuario/configuracao',                     [UsuarioController::class, 'configuracoes'])->name('users.configuracoes');
    Route::put('/usuario/trocaSenha',                       [UsuarioController::class, 'trocaSenha'])->name('users.trocaSenha');

    //Changelog
    Route::get('/changelog',                                [SystemController::class, 'changelog'])->name('changelog');

    Route::middleware(['isAdmin'])->group(function () {
        //Empresa
        Route::get('/empresa',                              [EmpresaController::class, 'index'])->name('business');
        Route::get('/empresa/criar/{id?}',                  [EmpresaController::class, 'create'])->name('business.create');
        Route::post('/empresa/criar/{id?}',                 [EmpresaController::class, 'create'])->name('business.createPost');

        //Usuário
        Route::get('/usuario',                              [UsuarioController::class, 'index'])->name('users');
        /*Route::get('/usuario/criar/{id?}',                [EmpresaController::class, 'create'])->name('business.create');
        Route::post('/usuario/criar/{id?}',                 [EmpresaController::class, 'create'])->name('business.createPost');*/
    });
});

/*Route::domain('{empresa}.' . env('APP_URL'))->group(function () {
});*/
